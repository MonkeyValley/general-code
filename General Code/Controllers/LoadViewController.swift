//
//  LoadViewController.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import UIKit
import  Lottie
import RealmSwift

class LoadViewController: UIViewController {

    
    @IBOutlet weak var lottieView:AnimationView!
    @IBOutlet weak var lblDataLoad:UILabel!
    
    private let realm = try! Realm()
    private var user:UserModel?
    private let sessionUser = UserDefaults.standard
    private let alert = JojoAlert()
    var from = "load"
    private var getDevicesIsEnd = false
    private var getParentDevicesIsEnd = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        getUserData()
        initUI()

    }
    
    func restartDevices()  {
        self.lblDataLoad.text = "Consultando datos..."
        requestHelper.searchDevices(userId: user?._id ?? "") { result in
            if(result != nil){
                _ = Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: { (t) in
                    self.getDevices()
                    self.getParentDevices()
                })
            }else{
                self.alert.showToast(message: "No se pudo conectar con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
            }
        }
    }
    
    func getUserData()  {
            
        let loadedData = sessionUser.object(forKey: "_userData") as! String
        let jsonData = Data(loadedData.utf8)

        do{
            let decoder = JSONDecoder()
            self.user = try decoder.decode(UserModel.self, from: jsonData)

        }catch let error as NSError{
            debugPrint("LOGIN ERROR",error)
        }
    }
    
    func initUI()  {
        
        let lottieAnim = AnimationView.init(name: "anim_circle_download")
        lottieAnim.frame =  CGRect(x: 0, y: 0, width: lottieView.frame.width, height: lottieView.frame.height)
        lottieAnim.contentMode = .scaleAspectFit
        lottieAnim.animationSpeed = 0.8
        lottieAnim.loopMode = .loop
        lottieAnim.play()
        lottieView.addSubview(lottieAnim)
        restartDevices()
        print(Realm.Configuration.defaultConfiguration.fileURL!)

    }
    
    func getDevices()  {
        lblDataLoad.text = "Obteniendo dispositivos hijos..."
        requestHelper.getListDevices(userId: user!._id , completion: { result in
            if result != nil{
                
                try! self.realm.write{
                    let deviceExist = self.realm.objects(DeviceModelRelm.self)
                    self.realm.delete(deviceExist)
                }
                
                try! self.realm.write{
                    result!.forEach{ it in
                        let model = DeviceModelRelm()
                        model._id = it._id
                        model.childId = it.childId
                        model.childType = it.childType ?? -1
                        model.deviceName = it.deviceName ?? ""
                        model.notification = it.notification
                        model.status = it.status
                        model.sensor = it.sensor
                        model.user = it.user
                        
                        self.realm.add(model)
                    }
                    self.getDevicesIsEnd = true
                    self.goToHome()
                }
            }else{
                self.alert.showToast(message: "No se pudo conectar con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: "xmark.circle.fill", ctx: self)
            }
        })
    }
    func getParentDevices() {
        lblDataLoad.text = "Obteniendo dispositivos padres..."
        
        requestHelper.getParentDevices(userId: user!._id) { result in
            if(result != nil){
                try! self.realm.write{
                    let deviceExist = self.realm.objects(ParentDeviceModelRealm.self)
                    let deviceChild = self.realm.objects(ParentChildDevicesModelRealm.self)
                    
                    self.realm.delete(deviceExist)
                    self.realm.delete(deviceChild)
                }
                
                try! self.realm.write{
                    
                    result?.forEach{it in
                        let modelParent = ParentDeviceModelRealm()
                        modelParent.SSID = it.SSID
                        modelParent._id = it._id
                        modelParent.childs = it.childs
                        modelParent.deviceType = it.deviceType
                        modelParent.macAddress = it.macAddress
                        modelParent.status = it.status
                        modelParent.user = it.user
                        
                        it.userDevices.forEach{ child in
                            let modelChild = ParentChildDevicesModelRealm()
                            modelChild._id = child._id
                            modelChild._idParent = it._id
                            modelChild.childId = child.childId
                            modelChild.childType = child.childType ?? 0
                            modelChild.deviceName = child.deviceName ?? ""
                            modelChild.notification = child.notification
                            modelChild.status = child.status
                            modelChild.user = child.user
                            self.realm.add(modelChild)

                        }
                        
                        self.realm.add(modelParent)
                    }
                }
                self.getParentDevicesIsEnd = true
                self.goToHome()
            }else{
                self.alert.showToast(message: "No se pudo conectar con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: "xmark.circle.fill", ctx: self)
            }
            
        }
    }
    func goToHome()  {
        
        if getDevicesIsEnd && getParentDevicesIsEnd {
            let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let rootViewController = mainStoryBoard.instantiateViewController(withIdentifier: "tabbarhome") as! UITabBarController
            let vc = rootViewController.viewControllers![0] as! HomeViewController
            vc.dataFrom = from
            if let w = UIApplication.shared.keyWindow {
                w.rootViewController = rootViewController
                UIApplication.shared.keyWindow?.makeKeyAndVisible()
            }
        }
    }

}
