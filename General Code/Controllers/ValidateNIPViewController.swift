//
//  ValidateNIPViewController.swift
//  General Code
//
//  Created by TritonSoft on 13/05/21.
//

import UIKit
import Lottie

class ValidateNIPViewController: UIViewController {

    @IBOutlet weak var animView:AnimationView!
    @IBOutlet weak var txtNIP:UITextField!
    @IBOutlet weak var txtNewPassword:UITextField!
    
    var idRestPassword:String?
    private let alert = JojoAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUI()
    }
    
    func initUI(){
        
        let lottieAnim = AnimationView.init(name: "anim_send_mail")
        lottieAnim.frame =  animView.bounds
        lottieAnim.contentMode = .scaleAspectFit
        lottieAnim.animationSpeed = 0.5
        lottieAnim.loopMode = .loop
        lottieAnim.play()
        animView.addSubview(lottieAnim)
        
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 44.0))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let barButton = UIBarButtonItem(title: "Siguiente", style: .plain, target: self, action: #selector(self.actionReturnNip))
        toolBar.setItems([flexible, barButton], animated: true)
        self.txtNIP.inputAccessoryView = toolBar
        
    }
    func shouldReturn(textField:UITextField){
        if(txtNIP == textField){
            txtNewPassword.becomeFirstResponder()
        }else if(txtNewPassword == textField){
            txtNewPassword.resignFirstResponder()
        }
    }
    @objc func actionReturnNip(){
        shouldReturn(textField: txtNIP)
    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func validateCode(_ sender:UIButton){
        
        let nip = txtNIP.text!
        let newPass = txtNewPassword.text!
        
        if nip != "" && newPass != ""{
            
            requestHelper.validatePin(_id: self.idRestPassword ?? "", code: nip, newPassword: newPass) { (result) in
                if result != nil{
                    if !result!.bError{
                        self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_SUCCESS, ctx: self)
                        _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (t) in
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "resetresponsecontroller") as! ResetPasswordResponseViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                        })
                    }else{
                        self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                    }
                }else{
                    self.alert.showToast(message: "Error al contactar al servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
                }
            }
        }else{
        
            alert.showToast(message: "Falta proporcionar algunos datos", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
        }
        
        
    }
}
extension ValidateNIPViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.shouldReturn(textField: textField)
        return true
    }
}
