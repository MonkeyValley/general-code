//
//  LoginViewController.swift
//  General Code
//
//  Created by TritonSoft on 11/05/21.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var txtPhone:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    
    private let alert = JojoAlert()
    private var sharedPref = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    func initUI() {
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 44.0))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let barButton = UIBarButtonItem(title: "Siguiente", style: .plain, target: self, action: #selector(self.actionReturnPhone))
        toolBar.setItems([flexible, barButton], animated: true)
        self.txtPhone.inputAccessoryView = toolBar
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.actionHideKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    func shouldReturn(textField:UITextField) {
        if(txtPhone == textField){
            txtPassword.becomeFirstResponder()
        }else if (txtPassword == textField){
            txtPassword.resignFirstResponder()
        }
    }
    @objc func actionReturnPhone(){
        shouldReturn(textField: txtPhone)
    }
    @objc func actionHideKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func actionForgotPassword(_ sender:UIButton){
    
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "forgotpasswordnav") as! UINavigationController
//        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func login(_ sender:UIButton){
        let phone = txtPhone.text!
        let pass = txtPassword.text!
        
        
        requestHelper.loginPost(telefono: phone, password: pass, playerId: "") { (result) in
            
            if let res = result {
                if(!res.bError){
                    do {
                        
                        let jsonEncoder = JSONEncoder()
                        let jsonData = try jsonEncoder.encode(res.data!)
                        let json = String(data: jsonData, encoding: String.Encoding.utf8)
                        
                        
                        self.sharedPref.set(json, forKey: "_userData")
                        self.sharedPref.synchronize()
                            
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "loadcontroller") as! LoadViewController
                        vc.from = "login"
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        
                        /*let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let rootViewController = mainStoryBoard.instantiateViewController(withIdentifier: "tabbarhome") as! UITabBarController
                        print(rootViewController.viewControllers)
                        let vc = rootViewController.viewControllers![0] as! HomeViewController
                        vc.dataFrom = "login"
                        if let w =  UIApplication.shared.keyWindow {
                            w.rootViewController = rootViewController
                            UIApplication.shared.keyWindow?.makeKeyAndVisible()
                        }*/
                    }catch let error as NSError {
                            
                        let URL = Constantes.SERVER_URL + "/login"
                        self.alert.showToast(message: "Error a convertir los datos" , font: UIFont.systemFont(ofSize: 12, weight: .bold), UIIcon: "lock.circle.fill", ctx: self)
                        debugPrint(URL,error)
                    }
               
                }else{
                    self.alert.showToast(message: res.cMensaje, font: UIFont.systemFont(ofSize: 12, weight: .bold), UIIcon: "info.circle.fill", ctx: self)
                }
            }else{
                self.alert.showToast(message: "Error en la petición al servidor.", font: UIFont.systemFont(ofSize: 12, weight: .bold), UIIcon: "xmark.circle.fill", ctx: self)
            }
        }
        
    }

}
extension LoginViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        shouldReturn(textField: textField)
        return true
    }
}
