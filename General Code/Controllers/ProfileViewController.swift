//
//  ProfileViewController.swift
//  General Code
//
//  Created by TritonSoft on 11/05/21.
//

import UIKit
import Lottie
class ProfileViewController: UIViewController {
    
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblUserAddress:UILabel!
    
    private var user:UserModel!
    private let sessionUser = UserDefaults.standard
    private let sessionVar = UserDefaults.standard
    private let lottieAnim = AnimationView.init(name: "anim_profile_grey")

    private var options = [[String:String]]()
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        getUserData()
    }
    
    func getUserData()  {
        
         let loadedData = sessionUser.object(forKey: "_userData") as! String
        let jsonData = Data(loadedData.utf8)

        do{
            let decoder = JSONDecoder()
            user = try decoder.decode(UserModel.self, from: jsonData)
            lblUserName.text = "\(user.nombres) \(user.apellidos)"

            if user.lstDirecciones != nil && user.lstDirecciones?.count ?? 0 > 0{
                let address = "\(user.lstDirecciones![0].nombreCalle) \(user.lstDirecciones![0].numeroExterior), \(user.lstDirecciones![0].colonia)"
                lblUserAddress.text = address
            }else{
                lblUserAddress.text = ""
            }
            
            
            if user.tipoUsuario == 1{
                options = [
                    ["opc":"0", "label":"Agregar dispositivo", "image":""],
                    ["opc":"1","label":"Mis dispositivos", "image":""],
                    ["opc":"2","label":"Control de usuarios", "image":""],
                    ["opc":"3","label":"Sincronizar con alexa", "image":""],
                    ["opc":"4","label":"Soporte", "image":""],
                    ["opc":"5","label":"Cerrar sesión", "image":""]
                ]
            }else{
                options = [
                    ["opc":"3", "label":"Sincronizar con alexa", "image":""],
                    ["opc":"4", "label":"Soporte", "image":""],
                    ["opc":"5", "label":"Cerrar sesión", "image":""]
                ]
            }
            
        }catch let error as NSError{
            debugPrint("LOGIN ERROR",error)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        lottieAnim.play()
    }
    
    func initUI(){
       
    }

}
extension ProfileViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profilecell", for: indexPath)
        
        
        
        cell.textLabel?.text = options[indexPath.row]["label"]
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        cell.textLabel?.textColor = .systemGray2
        
        switch options[indexPath.row]["opc"]! {
        case "0":
            cell.imageView?.image = UIImage(systemName: "externaldrive.fill.badge.checkmark")
            cell.imageView?.tintColor = .systemGray2
            break
        case "1":
            cell.imageView?.image = UIImage(systemName: "house.fill")
            cell.imageView?.tintColor = .systemGray2
            break
        case "2":
            cell.imageView?.image = UIImage(systemName: "person.2.circle.fill")
            cell.imageView?.tintColor = .systemGray2
            break
        case "3":
            cell.imageView?.image = UIImage(named: "ic_alexa")
            cell.contentMode = .center
            break
        case "4":
            cell.imageView?.image = UIImage(systemName: "headphones.circle.fill")
            cell.imageView?.tintColor = .systemGray2
            break
        case "5":
            cell.imageView?.image = UIImage(systemName: "greetingcard.fill")
            cell.imageView?.tintColor = .systemGray2
            break
        default:
            cell.imageView?.image = UIImage(systemName: "photo.on.rectangle.angled")
            cell.imageView?.tintColor = .systemGray2
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = options[indexPath.row]
        switch item["opc"]! {
        case "0":

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "adddevicesnavcontroller") as! UINavigationController
            self.present(vc, animated: true, completion: nil)
            break
        case "1":
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "mydevicesnavigation") as! UINavigationController
            self.present(vc, animated: true, completion: nil)
            break
        case "2":
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "userscontrollController") as! UserControllViewController
            self.present(vc, animated: true, completion: nil)
            
            break
        case "3":
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "syncroniceAlexaController") as! SyncronizeAlexaViewController
            self.present(vc, animated: true, completion: nil)
            break
            
        case "5":
            
            sessionVar.removeObject(forKey: "_userData")
            sessionVar.synchronize()
            
            let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let rootViewController = mainStoryBoard.instantiateViewController(withIdentifier: "loginNavController") as! UINavigationController
            if let w =  UIApplication.shared.keyWindow {
                w.rootViewController = rootViewController
                UIApplication.shared.keyWindow?.makeKeyAndVisible()
            }
            
        default:
            break
        }
        
    
        
    }
}
