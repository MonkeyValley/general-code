//
//  ResetPasswordResponseViewController.swift
//  General Code
//
//  Created by TritonSoft on 13/05/21.
//

import UIKit
import Lottie

class ResetPasswordResponseViewController: UIViewController {

    
    @IBOutlet weak var lottieView:AnimationView!
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initUI()
    }
    
    func initUI(){
        print(lottieView.frame.width)
        let lottieAnim = AnimationView.init(name: "anim_ok")
        lottieAnim.frame =  CGRect(x: 0, y: 0, width: lottieView.frame.width, height: lottieView.frame.height)
        lottieAnim.contentMode = .scaleAspectFit
        lottieAnim.animationSpeed = 0.5
        lottieAnim.loopMode = .loop
        lottieAnim.play()
        lottieView.addSubview(lottieAnim)
    }
    

    @IBAction func actionClose(_ sender:UIButton){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController?.popToViewController(viewControllers[0], animated: true)
    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}
