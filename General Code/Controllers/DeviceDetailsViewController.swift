//
//  DeviceDetailsViewController.swift
//  General Code
//
//  Created by TritonSoft on 11/05/21.
//

import UIKit
import Lottie
import RealmSwift

class DeviceDetailsViewController: UIViewController {

    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblLastDate:UILabel!
    @IBOutlet weak var lblDeviceName:UILabel!
    @IBOutlet weak var lblDeviceStatus:UILabel!
    @IBOutlet weak var viewDeviceStatus:UIView!
    @IBOutlet weak var switchNotification:UISwitch!
    @IBOutlet weak var switchSensor:UISwitch!
    @IBOutlet weak var btnClose:UIButton!
    

    private let sessionUser = UserDefaults.standard
    private let alert = JojoAlert()
    private var user:UserModel?
    private var isReload = false
    private var lottieAnimOpen:AnimationView!
    private var lottieAnimClose:AnimationView!
    private let realm = try! Realm()
    var protocolo:DeviceDetailsDelegate?

    public var idDevice:DevicesModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        getUserData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        if(protocolo != nil && isReload == true){
            protocolo!.reloadHome()
        }
    }
    
    func initUI()  {
                
    }
    
    
    func getUserData()  {
        
        let loadedData = sessionUser.object(forKey: "_userData") as! String
        let jsonData = Data(loadedData.utf8)
        do{
            let decoder = JSONDecoder()
            user = try decoder.decode(UserModel.self, from: jsonData)
            lblUserName.text = user!.nombres
            lblLastDate.text = idDevice!.lastUserAccess
            lblDeviceName.text = idDevice!.deviceName
            switchNotification.setOn(idDevice!.notification, animated: true)
            switchSensor.setOn(idDevice!.sensor, animated: true)
            switch(idDevice?.status){
            case Constantes.STATUS_ABIERTO:
                lblDeviceStatus.text = "Abierto"
                btnClose.tag = 2
                viewDeviceStatus.backgroundColor = UtilClass().hexStringToUIColor(hex: "#009688")
                break
            case Constantes.STATUS_CERRADO:
                lblDeviceStatus.text = "Cerrado"
                btnClose.tag = 1
                viewDeviceStatus.backgroundColor = UtilClass().hexStringToUIColor(hex: "#009688")
                break
            default:
                btnClose.isEnabled = false
                btnClose.backgroundColor = .lightGray
                lblDeviceStatus.text = "Sin conexión"
                viewDeviceStatus.backgroundColor = .red
                break
                
            }
            
            
        }catch let error as NSError{
            debugPrint("LOGIN ERROR",error)
        }
        
    }
    
    @IBAction func actionEnabledSensors(_ sender:UISwitch){
        requestHelper.setSensorsActive(deviceId: idDevice?._id ?? "", enabled: sender.isOn) { response in
            guard let bError = response?.bError else{
                sender.isOn = !sender.isOn
                return
            }
            
            
            var message = ""
            var icon = ""
            if(sender.isOn){
                if(bError){
                    message = "El sensor no se ha podido activar"
                    icon = Constantes.ICON_ERROR
                    sender.isOn = false
                }else{
                    message = "El sensor se ha activado"
                    icon = Constantes.ICON_SUCCESS
                }
            }else{
                if(bError){
                    message = "El sensor no se ha podido desactivar"
                    icon = Constantes.ICON_ERROR
                    sender.isOn = true
                }else{
                    message = "El sensor se ha desactivado"
                    icon = Constantes.ICON_SUCCESS
                }
            }
            
            self.alert.showToast(message: message, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: icon, ctx: self)
            
        }
    }
    
    @IBAction func actionNotifications(_ sender:UISwitch){
        requestHelper.activeNotificationPost(macAddress: idDevice!._id, status: sender.isOn, completion: { result in
            if(result == nil){
                sender.isOn = !sender.isOn
                self.alert.showToast(message: "Error al desactivar notificaciones", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
            }else{
                self.isReload = true
            }
        })
    }
    
    @IBAction func actionGetManteiner(_ sender:UIButton){
        
    }
    
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionDelete(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "mydevicesnavigation") as! UINavigationController
        self.present(vc, animated: true, completion: nil)
        print(idDevice?._id)
        
    }
    
    @IBAction func actionActionDevice(_ sender:UIButton ){
        let tag = sender.tag
        self.btnClose.isEnabled = false

            requestHelper.deviceActionPost(userDevice: idDevice!._id, status: tag) { result in
                self.btnClose.isEnabled = true

                if result != nil{
                    if result!.action != nil{
                        if result?.action == 1 {
                                                            
                                HapticsManager.shared.vibrate(for: .success)
                                self.isReload = true
                                self.idDevice!.status = 1
                                self.lblDeviceStatus.text = "Abierto"
                            self.btnClose.tag = 2
                                self.alert.showToast(message: "El portón esta abriendo ", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_SUCCESS, ctx: self)
                            
                        }else if result?.action == 2 {
                            
                                HapticsManager.shared.vibrate(for: .success)
                                
                                self.isReload = true
                                self.idDevice!.status = 2
                            self.btnClose.tag = 1
                                self.lblDeviceStatus.text = "Cerrado"
                                
                                
                                self.alert.showToast(message: "El portón esta cerrando ", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_SUCCESS, ctx: self)
                            
                        }else{
                            self.lblDeviceStatus.text = "Sin conexión"
                            self.alert.showToast(message: "Accción no reconocida", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                        }
                        
                    }else{
                        self.alert.showToast(message: result!.cMensaje ?? "Dispositivo no encontrado", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                    }
                }else{
                    self.alert.showToast(message: "No se pudo conectar con el servidor.", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
                }
            }
       
    }
}
