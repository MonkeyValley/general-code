//
//  ScannQRViewController.swift
//  General Code
//
//  Created by TritonSoft on 10/05/21.
//

import AVFoundation
import UIKit

class ScannQRViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate{

    @IBOutlet var viewScanner:UIView!
    
    private var captureSession: AVCaptureSession!
    private var previewLayer: AVCaptureVideoPreviewLayer!
    private var alert = JojoAlert()
    var protocolo:ResultScannDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.systemBackground
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        
        viewScanner.layer.addSublayer(previewLayer)


        captureSession.startRunning()
    }
    func failed() {
            let ac = UIAlertController(title: "Escaneo no compatible", message: "Su dispositivo no admite escanear un código de un elemento. Utilice un dispositivo con cámara.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default){ it in
            self.dismiss(animated: true, completion: nil)
        })
            present(ac, animated: true)
            captureSession = nil
    }

        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

            if (captureSession?.isRunning == false) {
                captureSession.startRunning()
            }
        }

        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)

            if (captureSession?.isRunning == true) {
                captureSession.stopRunning()
            }
        }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }

//        dismiss(animated: true)
    }

    func found(code: String) {
        print(code)

        requestHelper.validateGuestUser(idGuestUser: code) { result in
            if(result != nil){
                if(result!.bError){
                    self.dismiss(animated: true) {
                        self.protocolo?.response(res: false, userid:"")
                    }
                }else{
                    self.dismiss(animated: true) {
                        self.protocolo?.response(res: true, userid:code)
                    }
                }
            }else{
                self.alert.showToast(message: "Error al conectar con el servidor.", font: .systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
            }
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}


