//
//  GetPasswordViewController.swift
//  General Code
//
//  Created by TritonSoft on 13/05/21.
//

import UIKit

class GetPasswordViewController: UIViewController {

    @IBOutlet weak var txtEmail:UITextField!
    
    private var alert = JojoAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionReset(_ sender:UIButton){
        
        let email = txtEmail.text!
        
        if email != ""{
            requestHelper.resetPassword(mail: email) { resul in
                if resul != nil{
                    
                    if !resul!.bError{
                        
    //                    validatenipcontroller
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "validatenipcontroller") as! ValidateNIPViewController
                        vc.idRestPassword = resul!._id ?? ""
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }else{
                        self.alert.showToast(message: resul!.cMensaje ?? "Error al procesar información.", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
                    }
                    
                }else{
                    self.alert.showToast(message: "Error al conectar con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
                }
            }
        }else{
            self.alert.showToast(message: "Proporcione su correo.", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
        }
    }

}
extension GetPasswordViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
