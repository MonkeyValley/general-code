//
//  UserControllViewController.swift
//  General Code
//
//  Created by TritonSoft on 14/05/21.
//

import UIKit
import RealmSwift

class UserControllViewController: UIViewController {

    @IBOutlet weak var tableViewChildUsers:UITableView!
    private let realm = try! Realm()
    private var arrayUsersChilds:Results<UserChildModelRealm>?
    
    private var user:UserModel?
    private let sessionUser = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        getUserData()
    }
    func getUserData()  {
        
        let loadedData = sessionUser.object(forKey: "_userData") as! String
        let jsonData = Data(loadedData.utf8)

        do{
            let decoder = JSONDecoder()
            user = try decoder.decode(UserModel.self, from: jsonData)
           
        }catch let error as NSError{
            debugPrint("LOGIN ERROR",error)
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        getChildUsers()
    }
    
    func getChildUsers() {
        arrayUsersChilds = self.realm.objects(UserChildModelRealm.self)
        print(arrayUsersChilds)
        tableViewChildUsers.reloadData()
    }
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}

extension UserControllViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrayUsersChilds?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("UsersAdapterTableViewCell", owner: self, options: nil)?.first as! UsersAdapterTableViewCell
        
        if indexPath.row == 0{
            cell.imgUser.image = UIImage(systemName: "plus")
            cell.imgUser.tintColor = .link
            
            cell.lblUserName.text = "Agregar usuario"
            cell.lblUserName.textColor = UIColor.link
            
            cell.accessoryType = .none
        }else{
            let item = arrayUsersChilds?[indexPath.row - 1]
            cell.lblUserName.text = "\(item?.nombres ?? "") \(item?.apellidos ?? "")"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row == 0){
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "registerusercontroller") as! RegisterUserViewController
            vc.protocolo = self

            self.present(vc, animated: true, completion: nil)
        }else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "userDetailsController") as! UserDetailsViewController
            let item = arrayUsersChilds![indexPath.row - 1]
            let userModel = UserModel(lstDirecciones: [AddressModel](), childUsers: [UserModel](), _id: item._id, nombres: item.nombres, apellidos: item.apellidos, mail: self.user?.mail ?? "", telefono: item.telefono, tipoUsuario: item.tipoUsuario, maxUsers: 0, playerId: nil, mailAlexa: nil)
            vc.data = userModel
            vc.protocolo = self
            self.present(vc, animated: true, completion: nil)
        }
    }
}
extension UserControllViewController: CallbackAddChildUserDelegate, CallbackReloadDataDelegate{
    func reloadData() {
        self.getChildUsers()
    }
}
