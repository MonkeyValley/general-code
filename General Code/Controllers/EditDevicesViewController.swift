//
//  EditDevicesViewController.swift
//  General Code
//
//  Created by TritonSoft on 24/05/21.
//

import UIKit
import RealmSwift

class EditDevicesViewController: UIViewController {

    @IBOutlet weak var lblDeviceName:UILabel!
    @IBOutlet weak var tableViewEditDevices:UITableView!
    @IBOutlet weak var btnAdd:UIButton!
    @IBOutlet weak var btnEdit:UIButton!
    
    var data:ParentDeviceModel?
    var arrayDevices = [RegisterDeviceModel]()
    private var selectedChild:DevicesModel?
    private let realm = try! Realm()
    private var sessionUser = UserDefaults.standard
    private var user:UserModel?
    private var alert = JojoAlert()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getUserData()
        initUI()
    }
    
    func initUI()  {
        if let d = data {
            lblDeviceName.text = Constantes.getNameDevice(deviceType: d.deviceType)
        }
        checkNumberdevices()
    }
    
    func checkNumberdevices(){
        if arrayDevices.count == data!.childs {
            btnAdd.isHidden = true
        }else{
            btnAdd.isHidden = false
        }
    }
    
    func getUserData()  {
        
        let loadedData = sessionUser.object(forKey: "_userData") as! String
        let jsonData = Data(loadedData.utf8)

        do{
            let decoder = JSONDecoder()
            user = try decoder.decode(UserModel.self, from: jsonData)

        }catch let error as NSError{
            debugPrint("LOGIN ERROR",error)
        }
        
    }
    
    func getDevices()  {
        requestHelper.getListDevices(userId: user?._id ?? "") { result in
            if(result != nil){
                
                try! self.realm.safeWrite {
                    let rows = self.realm.objects(DeviceModelRelm.self)
                    self.realm.delete(rows)
                    
                    let rowsTwo = self.realm.objects(ParentChildDevicesModelRealm.self)
                    self.realm.delete(rowsTwo)
                }
                
                try! self.realm.safeWrite {
                    result?.forEach{ it in
                        let device = DeviceModelRelm()
                        device._id = it._id
                        device.childId = it.childId
                        device.childType = it.childType ?? -1
                        device.deviceName = it.deviceName ?? ""
                        device.lastUserAccess = it.lastUserAccess
                        device.notification = it.notification
                        device.status = it.status
                        device.user = it.user
                        
                        self.realm.add(device)
                        
                        let deviceChild = ParentChildDevicesModelRealm()
                        deviceChild._id = it._id
                        deviceChild._idParent = self.data!._id
                        deviceChild.childId = it.childId
                        deviceChild.childType = it.childType ?? -1
                        deviceChild.deviceName = it.deviceName ?? ""
                        deviceChild.notification = it.notification
                        deviceChild.status = it.status
                        deviceChild.user = it.user
                        
                        self.realm.add(deviceChild)
                    }
                   
                }

            }else{
                
                self.alert.showToast(message: "Error al conectar con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: "xmark.circle.fill", ctx: self)
            }
            
        }
    }
    
    
    func actionAddChildDevice(index:Int)  {
        requestHelper.sendNewDevicesPost(body: arrayDevices, userId: user!._id, deviceId: data!._id) { result in
            DispatchQueue.main.async {
                if result != nil{
                    if !result!.bError{
                            self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_SUCCESS, ctx: self)
                            self.getDevices()
                    
                        
                    }else{
                        self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                    }
                }else{
                    self.alert.showToast(message: "Error al conectarse con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
                }
            }
        }
    }
    
    

    @IBAction func actionEditItems(_ sender:UIButton){
        sender.isHidden = true
        
        requestHelper.sendNewDevicesPost(body: arrayDevices, userId: user!._id, deviceId: data!._id) { result in
            
            DispatchQueue.main.async {
                if result != nil{
                    if !result!.bError{
                        self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_SUCCESS, ctx: self)
                        self.getDevices()
                        _ = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { it in
                            self.dismiss(animated: true, completion: nil)
                        })
                    }else{
                        self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                    }
                }else{
                    self.alert.showToast(message: "Error al conectarse con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
                }
            }
        }
    }
    
    @IBAction func actionAddItem(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "addchilddevicecontroller") as! AddChildDeviceViewController
        vc.protocoloEdit = self
        vc.action = "agregar"
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}

extension EditDevicesViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayDevices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("ItemChildsDevcieTableViewCell", owner: self, options: nil)?.first  as! ItemChildsDevcieTableViewCell
        
        if(arrayDevices[indexPath.row].childType > 0){
            cell.lblNumberDevice.text = "Dispositivo \(indexPath.row + 1)"
            cell.lblDeviceSelected.text = "\(Constantes.gteEmojiTypeDevice(deviceType: arrayDevices[indexPath.row].childType)) \(arrayDevices[indexPath.row].deviceName)"
        }else{
            cell.lblNumberDevice.text = "Dispositivo \(indexPath.row)"
            cell.lblDeviceSelected.text = ""
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "addchilddevicecontroller") as! AddChildDeviceViewController
        vc.protocoloEdit = self
        vc.action = "editar"
        vc.indexSelected = indexPath.row
        vc.data = arrayDevices[indexPath.row]
        self.present(vc, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
}
extension EditDevicesViewController: CallbackEditTypeDeviceDelegate{
    
    func selectedTypeEdit(item: RegisterDeviceModel, action: String, itemSeleted:Int) {
        if action == "agregar"{
            arrayDevices.append(item)
            checkNumberdevices()
            self.actionAddChildDevice(index:itemSeleted)
            tableViewEditDevices.reloadData()
            
        } else if action == "editar"{
            var indexpath = -1
            if itemSeleted >= 0{ indexpath = itemSeleted}
            arrayDevices[indexpath] = item
            tableViewEditDevices.reloadData()
            btnEdit.isHidden = false
        }
    }
}
