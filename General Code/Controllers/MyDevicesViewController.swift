//
//  MyDevicesViewController.swift
//  General Code
//
//  Created by TritonSoft on 14/05/21.
//

import UIKit
import RealmSwift
import Lottie

class MyDevicesViewController: UIViewController {

    
    @IBOutlet weak var tableViewParents:UITableView!
    
    private let alert = JojoAlert()
    private let realm = try! Realm()
    private let sessionUser = UserDefaults.standard
    
    private var user:UserModel?
    private var parentDevices = [ParentDeviceModel]()
    private var lottieAnim:AnimationView!

    private let refreshControl :UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector( actionRefreshControl(sender:) ), for:  .valueChanged)
        return refresh
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getDataDevices(type: 0)
    }
    func getDataDevices(type:Int){
        
        if(type == 0){
            let devicesArr = self.realm.objects(ParentDeviceModelRealm.self)
            if devicesArr.count > 0{
                devicesArr.forEach { it in
                    let deviceChild = self.realm.objects(ParentChildDevicesModelRealm.self).filter("_idParent == %@", it._id)
                    var childDevices = [DevicesModel]()
                    deviceChild.forEach{child in
                        let model = DevicesModel(_id: child._id, user: child.user, deviceName: child.deviceName, childType: child.childType, notification: child.notification, status: child.status, sensor: child.sensor, childId: child.childId, lastUserAccess: nil)
                        childDevices.append(model)
                    }
                    let model = ParentDeviceModel(_id: it._id, macAddress: it.macAddress, deviceType: it.deviceType, SSID: it.SSID, status: it.status, childs: it.childs, user: it.user, userDevices: childDevices)
                    
                    self.parentDevices.append(model)
                }
                self.tableViewParents.reloadData()
            }else{
               
            
               //lottieAnim.play()
            }
        }else{
            
        }
        
    }
    @objc func actionRefreshControl(sender:UIRefreshControl){
        getDataDevices(type: 1)
    }
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}

extension MyDevicesViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parentDevices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("DeviceAdapterTableViewCell", owner: self, options: nil)?.first  as! DeviceAdapterTableViewCell
        let item = parentDevices[indexPath.row]
        cell.lblDeviceName.text = Constantes.getNameDevice(deviceType: item.deviceType)
        cell.lblWifiName.text = item.SSID
        cell.lblNumbreDevices.text = "\(item.userDevices.count) dispositivos"
        if(item.status > 0 ){
            cell.lblStatus.text = "Conectado"
            cell.viewStatus.backgroundColor = UtilClass().hexStringToUIColor(hex: "#009688")
            
        }else{
            cell.lblStatus.text = "Sin conexión"
            cell.viewStatus.backgroundColor = UIColor.red
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = parentDevices[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "editdevicecontroller") as! EditDevicesViewController
        vc.data = item
        var arrSend = [RegisterDeviceModel]()
        item.userDevices.forEach{ it in
            
            let model = RegisterDeviceModel(deviceName: it.deviceName ?? "", childType: it.childType ?? 0)
            arrSend.append(model)
        }
        
        vc.arrayDevices = arrSend
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162.0
    }
    
}
