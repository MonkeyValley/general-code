//
//  ScannerLayerViewController.swift
//  General Code
//
//  Created by TritonSoft on 10/05/21.
//

import UIKit
import Lottie

class ResultScannerViewController: UIViewController {

    var protocolo:ScannAgainDelegate?
    
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var viewANimation:UIView!
    @IBOutlet weak var btnAction:UIButton!
    @IBOutlet weak var lblDexription:UILabel!
    @IBOutlet weak var lblSubtitle:UILabel!
    @IBOutlet weak var viewInput:UIView!
    
    private var sharedPref = UserDefaults.standard
    private var alert = JojoAlert()
    var userID = ""
    var isCorrect = false
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewDidAppear(_ animated: Bool) {
        initUI()
    }
    func initUI(){
        let lottieAnim = AnimationView.init(name: "anim_qr")
        lottieAnim.frame.size = CGSize(width: viewANimation.frame.width, height: viewANimation.frame.width)
        lottieAnim.frame.origin = CGPoint(x: 0, y: 0)
        lottieAnim.contentMode = .scaleAspectFill
        lottieAnim.animationSpeed = 0.5
        lottieAnim.loopMode = .loop
        viewANimation.addSubview(lottieAnim)
        lottieAnim.play()
        
        if(isCorrect){
            viewInput.isHidden = false
            lblDexription.text = "Aquí podrás elegir la contraseña para tu cuenta General Code."
            lblSubtitle.text = "Bienvenido"
            btnAction.setTitle("Guardar contraseña", for: .normal)
            btnAction.setImage(UIImage.init(systemName: "directcurrent"), for: .normal)
        }else{
            viewInput.isHidden = true
            lblDexription.text = "al parecer no es un QR valido, puedes escanear otra vez desde el botón 'Leer Qr'."
            lblSubtitle.text = "Escanea tu codigo y registrate"
            btnAction.setTitle("Leer QR", for: .normal)
            btnAction.setImage(UIImage.init(systemName: "qrcode.viewfinder"), for: .normal)
        }
        
        
    }
    @IBAction func actionClose(_ sender:UIButton){
        
        if(sender.tag == 1){
            
            if isCorrect {
                
                let pass = txtPassword.text!
                
                requestHelper.SetPasswordGuestUser(userId: self.userID, password: pass, playerId: "123") { result in
                    if(result != nil){
                        
                        if !result!.bError{
                            do {
                                
                                let jsonEncoder = JSONEncoder()
                                let jsonData = try jsonEncoder.encode(result!.user!)
                                let json = String(data: jsonData, encoding: String.Encoding.utf8)
                                
                                
                                self.sharedPref.set(json, forKey: "_userData")
                                self.sharedPref.synchronize()
                                    
                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "loadcontroller") as! LoadViewController
                                vc.from = "login"
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                                
                            }catch let error as NSError {
                                    
                                let URL = Constantes.SERVER_URL + "/setPasswordGuestUser/\(self.userID)"
                                self.alert.showToast(message: "Error a convertir los datos" , font: UIFont.systemFont(ofSize: 12, weight: .bold), UIIcon: "lock.circle.fill", ctx: self)
                                debugPrint(URL,error)
                            }
                        }else{
                            self.alert.showToast(message: result!.cMensaje, font: .systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                        }
                        
                    }else{
                        self.alert.showToast(message: "Error al conectar con el servidor", font: .systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
                    }
                }
                
                
            }else{
                self.dismiss(animated: true){
                    self.protocolo?.scann()
                }
            }
            
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
}

extension ResultScannerViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
}
