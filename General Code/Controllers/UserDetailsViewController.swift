//
//  UserDetailsViewController.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import UIKit
import RealmSwift

class UserDetailsViewController: UIViewController {

    @IBOutlet weak var imgQr:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblMail:UILabel!
    @IBOutlet weak var lblPhone:UILabel!
    
    var alert = JojoAlert()
    var data:UserModel?
    private let realm = try! Realm()
    var protocolo:CallbackReloadDataDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    func initUI(){
        
        let image = generateQRCode(from: data?._id ?? "")
        imgQr.image = image
        
        lblName.text = "\(data?.nombres ?? "") \(data?.apellidos ?? "")"
        lblMail.text = data?.mail ?? ""
        lblPhone.text = data?.telefono ?? ""
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    
    @IBAction func actionDeleteChildUser(_ sender:UIButton){
        
        let leftButon = UIButton()
        let rightButon = UIButton()
        
        leftButon.backgroundColor = .red
        leftButon.addTarget(self, action: #selector(self.actionCancel(_:)), for: .touchUpInside)
        rightButon.addTarget(self, action: #selector(self.actionAccent(_:)), for: .touchUpInside)
        
        
        let desc = "Si eliminas al usuario ya no podrá utilizar los dispositivos General Code"
        alert.showAlertInfo(title: "¿Quieres eliminar?", description: desc, buttonTextLeft: "Cancelar", buttonTextRight: "Eliminar", type: "info", on: self, leftButton: leftButon, rightButton: rightButon)
        
    }
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @objc func actionAccent(_ sender:UIButton){
        let userID = data!._id
        requestHelper.deleteChildUser(userId: userID) { result in
            self.alert.dismissAlert()
            if result != nil{
                if !result!.bError{
                    
                    
                    self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_SUCCESS, ctx: self)
                    
                    
                    try! self.realm.safeWrite {
                        let objToDelete = self.realm.objects(UserChildModelRealm.self)
                        self.realm.delete(objToDelete)
                    }
                    
                    try! self.realm.safeWrite {
                        guard let childs = result!.user!.childUsers else{return}
                        childs.forEach{ child in
                            let model = UserChildModelRealm()

                            model._id = child._id
                            model.apellidos = child.apellidos
                            model.nombres = child.nombres
                            model.parentUser = result!.user!._id
                            model.telefono = child.telefono
                            model.tipoUsuario = child.tipoUsuario
                            
                            self.realm.add(model)
                        }
                    }
                    
                    _ = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { (t) in
                        self.dismiss(animated: true, completion: {
                            self.protocolo?.reloadData()
                        })
                    })
                    
                }else{
                    self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                }
            }else{
                self.alert.showToast(message: "Error al contactar con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
            }
        }
    }
    
    @objc func actionCancel(_ sender:UIButton){
        alert.dismissAlert()
    }

}
