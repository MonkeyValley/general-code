//
//  RegiterViewController.swift
//  General Code
//
//  Created by TritonSoft on 10/05/21.
//

import UIKit

class RegiterViewController: UIViewController {

    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtPhone:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var lblAddressName:UILabel!
    
    private let sessionVar = UserDefaults.standard
    private var addressRegister:AddressModel?
    private let alert = JojoAlert()
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        print(view.frame.height)
    }

    func initUI() {
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 44.0))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let barButton = UIBarButtonItem(title: "Siguiente", style: .plain, target: self, action: #selector(self.actionReturnPhone))
        toolBar.setItems([flexible, barButton], animated: true)
        self.txtPhone.inputAccessoryView = toolBar
    }
    func scannInit() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "scannqrcontroller") as! ScannQRViewController
        vc.protocolo = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func actionReturnPhone(){
        txtPassword.becomeFirstResponder()
    }
    
    @IBAction func actionScannQR(_ sender:UIButton){
       scannInit()
    }
    @IBAction func actionRegister(_ sender:UIButton){
        
        let names = txtName.text!
        let lastnames = txtLastName.text!
        let phone = txtPhone.text!
        let mail = txtEmail.text!
        let password = txtPassword.text!
        let playerID = (UIDevice.current.identifierForVendor?.uuidString) ?? ""
        
        
        
        
        if(names != "" && lastnames != "" && phone != "" && mail != "" && password != ""  ){
            if(addressRegister != nil){
//                let nombreDireccion = addressRegister!.nombreDireccion
//                let calleDireccion = addressRegister!.nombreCalle
//                let numExtDireccion = addressRegister!.numeroExterior
//                let numIntDireccion = addressRegister!.numeroInterior
//                let refDireccion = addressRegister!.referencias
//                let coloniaDireccion = addressRegister!.colonia
                
                
                requestHelper.registerUserPost(nombres: names, apellidos: lastnames, email: mail, telefono: phone, password: password, playerId: playerID, domicilio: addressRegister!) { result in
                    
                    if result != nil{
                        
                        if(!result!.bError){
                            
                            do {
                                
                                let jsonEncoder = JSONEncoder()
                                let jsonData = try jsonEncoder.encode(result!.user)
                                let json = String(data: jsonData, encoding: String.Encoding.utf8)
                                
                                
                                self.sessionVar.set(json, forKey: "_userData")
                                self.sessionVar.synchronize()
                                
                                self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 16, weight: .bold), UIIcon: "checkmark.circle.fill", ctx: self)
                                let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let rootViewController = mainStoryBoard.instantiateViewController(withIdentifier: "tabbarhome") as! UITabBarController
                                if let w =  UIApplication.shared.keyWindow {
                                    w.rootViewController = rootViewController
                                    UIApplication.shared.keyWindow?.makeKeyAndVisible()
                                }
                            }catch let error as NSError {
                                    
                                let URL = Constantes.SERVER_URL + "/login"
                                self.alert.showToast(message: "Error a convertir los datos" , font: UIFont.systemFont(ofSize: 12, weight: .bold), UIIcon: "xmark.circle.fill", ctx: self)
                                debugPrint(URL,error)
                            }
                    
                        }else{
                            self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 16, weight: .bold), UIIcon: "info.circle.fill", ctx: self)
                        }
                        
                    }else{
                        self.alert.showToast(message: "Error en la petición al servidor", font: UIFont.systemFont(ofSize: 16, weight: .bold), UIIcon: "server.rack", ctx: self)
                    }
                }
            }else{
                alert.showToast(message: "Es necesario agregar una dirección", font: UIFont.systemFont(ofSize: 16, weight: .bold), UIIcon: "location.circle.fill", ctx: self)
            }
        }else{
            alert.showToast(message: "Faltan algunos datos", font: UIFont.systemFont(ofSize: 16, weight: .bold), UIIcon: "xmark.circle.fill", ctx: self)
        }
        
    }
    @IBAction func actionRegisterAddress(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "registeraddresscontroller") as! RegisterAddressViewController
        vc.protocolo = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func actionOmit(_ sender:UIButton){
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "loginController") as! LoginViewController
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootViewController = mainStoryBoard.instantiateViewController(withIdentifier: "loginNavController") as! UINavigationController
        if let w =  UIApplication.shared.keyWindow {
            w.rootViewController = rootViewController
            UIApplication.shared.keyWindow?.makeKeyAndVisible()
        }
    }
}
extension RegiterViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtName{
            txtLastName.becomeFirstResponder()
        }else if textField == txtLastName{
            txtEmail.becomeFirstResponder()
        }else if textField == txtEmail{
            txtPhone.becomeFirstResponder()
        }else if textField == txtPhone{
            txtPassword.becomeFirstResponder()
        }else if textField == txtPassword{
            txtPassword.resignFirstResponder()
        }
        
        return true
    }
}
extension RegiterViewController: ResultScannDelegate, ScannAgainDelegate, CallbackResultDelegate{
    func resultRegisterAddres(address: AddressModel) {
        print(address)
        self.addressRegister = address
        self.lblAddressName.text = address.nombreDireccion
        self.alert.showToast(message: "Registrado correctamente", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: "checkmark.circle.fill", ctx: self)
    }
    
    func scann() {
        scannInit()
    }
    
    func response(res: Bool, userid:String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "scannresultcontroller") as! ResultScannerViewController
        vc.protocolo = self
        vc.isCorrect = res
        vc.userID = userid
        self.present(vc, animated: true, completion: nil)
    }
}
