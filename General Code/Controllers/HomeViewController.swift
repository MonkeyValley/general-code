//
//  HomeViewController.swift
//  General Code
//
//  Created by TritonSoft on 11/05/21.
//

import UIKit
import Lottie
import Foundation
import RealmSwift


class HomeViewController: UIViewController{

    @IBOutlet weak var collectionDevices:UICollectionView!
    @IBOutlet weak var lblUserNameHeader:UILabel!
    @IBOutlet weak var lottieView:UIView!
    @IBOutlet weak var emptyView:UIView!
    @IBOutlet weak var reloadData:UIImageView!
    
    private let alert = JojoAlert()
    private let realm = try! Realm()
    private let sessionUser = UserDefaults.standard
    private var currentChildUsers:Results<UserChildModelRealm>!
    var dataFrom = ""
    private var collectionviewFlowLayout:UICollectionViewFlowLayout!
    private var devices = [DevicesModel]()
    private var user:UserModel?
    private var lottieAnim:AnimationView!
    private let refreshControl :UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector( actionRefreshControl(sender:) ), for:  .valueChanged)
        return refresh
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        setupCollectionView()
        getUserData()
        getDataDevices(type: 0)
    }
    
    func getUserData()  {
        
         let loadedData = sessionUser.object(forKey: "_userData") as! String
        let jsonData = Data(loadedData.utf8)

        do{
            let decoder = JSONDecoder()
            user = try decoder.decode(UserModel.self, from: jsonData)
            lblUserNameHeader.text = String(user!.nombres.capitalized ) + " " + String(user!.apellidos.capitalized)
            
            if(dataFrom == "login"){
                self.getDataLogin()
            }
        }catch let error as NSError{
            debugPrint("LOGIN ERROR",error)
        }
        
    }
    
    func getDataLogin() {

        try! self.realm.safeWrite{
            let currentChildUsers = realm.objects(UserChildModelRealm.self)
            realm.delete(currentChildUsers)
        }
        
        try! self.realm.safeWrite{
            guard let childs = user?.childUsers else{return}
            childs.forEach{ child in
                let model = UserChildModelRealm()
                model._id = child._id
                model.apellidos = child.apellidos
                model.nombres = child.nombres
                model.parentUser = user!._id
                model.telefono = child.telefono
                model.tipoUsuario = child.tipoUsuario
                
                self.realm.add(model)
            }
            
        }
    }
    
    func getDataDevices(type:Int){
        
        if(type == 0){
            let devicesArr = self.realm.objects(DeviceModelRelm.self)
            if devicesArr.count > 0{
                
                collectionDevices.isHidden = false
                emptyView.isHidden = true
                lottieAnim.stop()
                devicesArr.forEach { it in
                    let model = DevicesModel(
                        _id: it._id,
                        user: it.user,
                        deviceName: it.deviceName,
                        childType: it.childType,
                        notification: it.notification,
                        status: it.status,
                        sensor: it.sensor,
                        childId: it.childId,
                        lastUserAccess: it.lastUserAccess ?? "")
                    
                    self.devices.append(model)
                }
            }else{
                collectionDevices.isHidden = true
                emptyView.isHidden = false
            
                lottieAnim.play()
                
            }
        }else{
            requestHelper.getListDevices(userId: user?._id ?? "") { result in
                if(result != nil){
                    self.devices = result ?? [DevicesModel]()
                    
                }else{
                    
                    self.alert.showToast(message: "Error al conectar con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: "xmark.circle.fill", ctx: self)
                }
                self.collectionDevices.reloadData()
                self.refreshControl.endRefreshing()
                
                if self.devices.count == 0{
                    self.collectionDevices.isHidden = true
                    self.emptyView.isHidden = false
                    self.lottieAnim.play()
                }else{
                    self.collectionDevices.isHidden = false
                    self.emptyView.isHidden = true
                }
            }
        }
        
    }
    func setupCollectionView() {
        
        let nib = UINib(nibName: "ItemDevicesCollectionViewCell", bundle: nil)
        collectionDevices.register(nib, forCellWithReuseIdentifier: "itemdevices")
        collectionDevices.refreshControl = refreshControl
    
    }
    override func viewWillLayoutSubviews() {
        setupCollectionViewItemSize()
    }
    func setupCollectionViewItemSize() {
        if collectionviewFlowLayout == nil {
            let numberitems:CGFloat = 2
            let lineSpacing:CGFloat = 5
            let interspacign:CGFloat = 5
            
            
            let widthF:CGFloat  = ((collectionDevices.frame.width - (numberitems - 1) * interspacign) / numberitems) - 20
            let heightF:CGFloat  = 170.0
  
            
            
            collectionviewFlowLayout = UICollectionViewFlowLayout()
            
            collectionviewFlowLayout.itemSize = CGSize(width: widthF, height: heightF)
            collectionviewFlowLayout.sectionInset = .zero
            collectionviewFlowLayout.scrollDirection = .vertical
            collectionviewFlowLayout.minimumLineSpacing = lineSpacing
            collectionviewFlowLayout.minimumInteritemSpacing = interspacign
            
            
            collectionDevices.setCollectionViewLayout(collectionviewFlowLayout, animated: true)
        }
        
        
    }
    
    func initUI(){
        
        guard let tabbar = self.tabBarController?.tabBar else {
            return
        }
        
        tabbar.unselectedItemTintColor = .label
        
        tabbar.layer.cornerRadius = 30
        tabbar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        tabbar.layer.masksToBounds = true
        tabbar.layer.borderWidth = 0.0
        
        lottieAnim = AnimationView.init(name: "anim_noWiFi")
        lottieAnim.frame =  CGRect(x: 0, y: 0, width: self.lottieView.frame.width, height: self.lottieView.frame.height)
        lottieAnim.contentMode = .scaleAspectFit
        lottieAnim.animationSpeed = 0.8
        lottieAnim.loopMode = .loop
        lottieAnim.play()
        self.lottieView.addSubview(lottieAnim)
        
        let tapReload = UITapGestureRecognizer(target: self, action: #selector(self.actionRefreshButton(sender:)))
        reloadData.addGestureRecognizer(tapReload)
    }
   
    @IBAction func actionAddAccount(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "adddevicesnavcontroller") as! UINavigationController
        self.present(vc, animated: true, completion: nil)
    }
    @objc func actionRefreshControl(sender:UIRefreshControl){
//        sender.endRefreshing()
        getDataDevices(type: 1)
    }
    @objc func actionRefreshButton(sender:UIButton){
//        sender.endRefreshing()
        getDataDevices(type: 1)
    }
}


extension HomeViewController:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return devices.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemdevices", for: indexPath) as! ItemDevicesCollectionViewCell
        let item = devices[indexPath.row]
        
        let lottieAnim = AnimationView.init(name: "dot_pulse")
        lottieAnim.frame =  CGRect(x: 0, y: 0, width: cell.viewStatusConected.frame.width, height: cell.viewStatusConected.frame.height)
        lottieAnim.contentMode = .scaleAspectFit
        lottieAnim.animationSpeed = 0.8
        lottieAnim.loopMode = .loop
        lottieAnim.play()
        cell.viewStatusConected.addSubview(lottieAnim)
        cell.viewStatusConected.backgroundColor = .systemBackground
        cell.lblStatusConected.text = "Buscando..."
        
        switch item.childType {
        case 1:
                cell.icTypeDevice.text = Constantes.STATUS_EMOJI_CHILD_TYPE_1
            break
        case 2:
                cell.icTypeDevice.text = Constantes.STATUS_EMOJI_CHILD_TYPE_2
            break
       
        default:
            cell.icTypeDevice.text = Constantes.STATUS_EMOJI_CHILD_TYPE_3
            break
        }
        
        _ = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block:{ t in
            
            lottieAnim.removeFromSuperview()
            switch(item.status){
            case Constantes.STATUS_ABIERTO:
                cell.lblStatusDevice.text = Constantes.STATUS_ABIERTO_EMOJI
                cell.lblStatusConected.text = "Abierto"
                cell.viewStatusConected.backgroundColor = UtilClass().hexStringToUIColor(hex: "#009688")
                break
            case Constantes.STATUS_CERRADO:
                cell.lblStatusDevice.text = Constantes.STATUS_CERRADO_EMOJI
                cell.lblStatusConected.text = "Cerrado"
                cell.viewStatusConected.backgroundColor = UtilClass().hexStringToUIColor(hex: "#009688")
                break
            default:
                cell.lblStatusDevice.text = Constantes.STATUS_OFFLINE_EMOJI
                cell.lblStatusConected.text = "Sin conexión"
                cell.viewStatusConected.backgroundColor = .red
                break
                
            }
        })
        
        cell.lblNameDevice.text = item.deviceName
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "deviceDetailsController") as! DeviceDetailsViewController
        vc.idDevice = devices[indexPath.row]
        vc.protocolo = self
        self.present(vc, animated: true, completion: nil)
    }
    
    
}
extension HomeViewController: DeviceDetailsDelegate{
    func reloadHome() {
        self.getDataDevices(type:1)
    }
}
extension Realm {
    public func safeWrite(_ block: (() throws -> Void)) throws {
        if isInWriteTransaction {
            try block()
        } else {
            try write(block)
        }
    }
}
