//
//  RegisterAddressViewController.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import UIKit
import AMPopTip


class RegisterAddressViewController: UIViewController {

    @IBOutlet weak var txtNameAddres:UITextField!
    @IBOutlet weak var txtStreet:UITextField!
    @IBOutlet weak var txtIntNum:UITextField!
    @IBOutlet weak var txtExtNum:UITextField!
    @IBOutlet weak var txtNeighborhood:UITextField!
    @IBOutlet weak var txtReferences:UITextField!
    @IBOutlet weak var viewNumInt:UIView!
    
    var protocolo:CallbackResultDelegate?
    private var alert = JojoAlert()
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }
    
    func initUI() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard))
        view.addGestureRecognizer(tap)
        
        
        let toolBarInt = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 44.0))
        let flexibleInt = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let barButtonInt = UIBarButtonItem(title: "Siguiente", style: .plain, target: self, action: #selector(self.closeInt(_:)))
        toolBarInt.setItems([flexibleInt, barButtonInt], animated: true)
        self.txtIntNum.inputAccessoryView = toolBarInt
        
        let toolBarExt = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 44.0))
        let flexibleExt = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let barButtonExt = UIBarButtonItem(title: "Siguiente", style: .plain, target: self, action: #selector(self.closeExt(_:)))
        toolBarExt.setItems([flexibleExt, barButtonExt], animated: true)
        self.txtExtNum.inputAccessoryView = toolBarExt
    }
    
    func shouldReturn(textField:UITextField){
        if(textField == txtNameAddres){
            txtStreet.becomeFirstResponder()
        }else if textField == txtStreet{
            txtExtNum.becomeFirstResponder()
        }else if textField == txtExtNum{
            txtIntNum.becomeFirstResponder()
        }else if textField == txtIntNum{
            txtNeighborhood.becomeFirstResponder()
        }else if textField == txtNeighborhood{
            txtReferences.becomeFirstResponder()
        }else if textField == txtReferences{
            txtReferences.resignFirstResponder()
        }
    }
    
    
    @objc func closeKeyboard(){
        view.endEditing(true)
    }
    
    @objc func closeInt(_ sender:UIButton){
        shouldReturn(textField: txtIntNum)
    }
    @objc func closeExt(_ sender:UIButton){
        shouldReturn(textField: txtExtNum)
    }
    
    @IBAction func actionRegister(_ sender:UIButton){
        
        let nameAddress = txtNameAddres.text!
        let street = txtStreet.text!
        let extNum = txtExtNum.text!
        let intNum = txtIntNum.text ?? ""
        let neigh = txtNeighborhood.text!
        let ref = txtReferences.text!
        
        if nameAddress != "" && street != "" && extNum != "" && neigh != "" && ref != "" {}
        if(protocolo != nil){
            let address = AddressModel(_id: "", nombreDireccion: nameAddress, nombreCalle: street, numeroExterior: extNum, numeroInterior: intNum, referencias: ref, colonia: neigh, _lat: 0.0, _lng: 0.0, user: "")
            protocolo!.resultRegisterAddres(address: address )
            self.dismiss(animated: true, completion: nil)
        }else{
            self.alert.showToast(message: "Faltan algunos datos", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: "checkmark.circle.fill", ctx: self)
        }
        
        
    }
    
    @IBAction func actionInfo(_ sender:UIButton){
        let popTip = PopTip()
        let frame = CGRect(x: 190, y: 210, width: 167.5, height: 50)
        popTip.show(text: "Opcional", direction: .down, maxWidth: 200, in: view, from: frame)
    }
    
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}

extension RegisterAddressViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.shouldReturn(textField: textField)
        
        return true
    }
}
