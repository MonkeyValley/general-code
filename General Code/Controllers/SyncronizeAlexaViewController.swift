//
//  SyncronizeAlexaViewController.swift
//  General Code
//
//  Created by TritonSoft on 14/05/21.
//

import UIKit

class SyncronizeAlexaViewController: UIViewController {

    @IBOutlet weak var txtUserMail:UITextField!
    
    var alert = JojoAlert()
    private var user:UserModel?
    private let sessionUser = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUserData()
    }

    func getUserData()  {
        
         let loadedData = sessionUser.object(forKey: "_userData") as! String
        let jsonData = Data(loadedData.utf8)

        do{
            let decoder = JSONDecoder()
            user = try decoder.decode(UserModel.self, from: jsonData)

        }catch let error as NSError{
            debugPrint("LOGIN ERROR",error)
        }
        
    }
    
    @IBAction func actionSyncronizeAlexa(_ sender:UIButton){
        let mail = txtUserMail.text!
        
        if(mail != ""){
            requestHelper.sendSyncronizeAlexa(userID: user!._id, mail: mail) { result in
                if(result != nil){
                    if(!result!.bError){
                        self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { timer in
                            self.dismiss(animated: true, completion: nil)
                        }
                    }else{
                        self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                    }
                }else{
                    self.alert.showToast(message: "No se pudo conectar con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
                }
            }
        }else{
            alert.showToast(message: "Proporciona tu correo", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
        }
    }
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}
extension SyncronizeAlexaViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
