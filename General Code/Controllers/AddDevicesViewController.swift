//
//  AddDevicesViewController.swift
//  General Code
//
//  Created by TritonSoft on 13/05/21.
//

import UIKit
import Lottie
import SystemConfiguration.CaptiveNetwork
import CoreLocation
import RealmSwift

class AddDevicesViewController: UIViewController, CLLocationManagerDelegate {
    
//    TODO: {{host}}/getNewDevices/{{SSID}}

    @IBOutlet weak var lottieView:AnimationView!
    @IBOutlet weak var emptyView:UIView!
    @IBOutlet weak var tableViewDevices:UITableView!
    @IBOutlet weak var lblNumberDevices:UILabel!
    @IBOutlet weak var reloadData:UIImageView!

    
    private var locationManager:CLLocationManager?
    private let alert = JojoAlert()
    private var arrayData = [NewDevicesModel]()
    private var sessionUser = UserDefaults.standard
    private var user:UserModel?
    private let realm = try! Realm()

    override func viewDidLoad() {
        super.viewDidLoad()
        showTutorial()
    }
    

    override func viewDidAppear(_ animated: Bool) {
        initUI()
    }
    
    func initUI()  {
        
        let lottieAnim = AnimationView.init(name: "anim_noWiFi")
        lottieAnim.frame =  CGRect(x: 0, y: 0, width: lottieView.frame.width, height: lottieView.frame.height)
        lottieAnim.contentMode = .scaleAspectFit
        lottieAnim.animationSpeed = 0.5
        lottieAnim.loopMode = .loop
        lottieAnim.play()
        
        
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager?.distanceFilter = 1000.0
        self.locationManager?.requestWhenInUseAuthorization()
        self.locationManager?.requestAlwaysAuthorization()
        self.locationManager?.startUpdatingLocation()
        
        
        lottieView.addSubview(lottieAnim)
//        getNewDevices(SSID: self.getWiFiName() ?? "")
        getNewDevices(SSID:"Telcel-7C6F")
        let tapReload = UITapGestureRecognizer(target: self, action: #selector(self.actionRefreshButton(sender:)))
        reloadData.addGestureRecognizer(tapReload)
        
        getUserData()
    }
    func showTutorial(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "onboardingContoller") as! ViewController
        vc.from = "addDevice"
        vc.sliders = [SliderModel(
                        title: "Hola de nuevo",
                        description: "",
                        position: nil,
                        animationName: "hello",
                        backgroundColor: .systemBackground),
                      SliderModel(
                        title: "Comencemos la configuracion",
                        description: "El primer paso para comezar es verificar que tu tecnico haya instalado y encendido tu dispositivo General Code.",
                        position: nil,
                        animationName: "config",
                        backgroundColor: .systemBackground),
                      SliderModel(
                        title: "Conectar el dispositivo al Wifi",
                        description: "Te recomendamos usar un telefono diferente al que estas usando para conectar tu nuevo dispositivo a la red Wifi de tu hogar. Ahora tu dispositivo emitira una red Wifi llamada General Code, conectate y agrega el nombre de la red y el password del modem.",
                        position: nil,
                        animationName: "wifi",
                        backgroundColor: .systemBackground),
                      SliderModel(
                        title: "Importante",
                        description: "Te recomendamos tener el password de tu modem a la mano.",
                        position: nil,
                        animationName: "password",
                        backgroundColor: .systemBackground),
                      SliderModel(
                        title: "Ya casi terminamos",
                        description: "Ahora tu dispositivo emitira una luz verde indicando que ya esta disponible dentro de tu red Wifi.",
                        position: nil,
                        animationName: "luz_verde",
                        backgroundColor: .systemBackground),
                      SliderModel(
                        title: "Listo",
                        description: "Ahora solo queda personalizar tu dispositivo.",
                        position: nil,
                        animationName: "done",
                        backgroundColor: .systemBackground)
        ]
        self.present(vc, animated: true, completion: nil)
    }
    func getUserData()  {
        
        let loadedData = sessionUser.object(forKey: "_userData") as! String
        let jsonData = Data(loadedData.utf8)

        do{
            let decoder = JSONDecoder()
            user = try decoder.decode(UserModel.self, from: jsonData)

        }catch let error as NSError{
            debugPrint("LOGIN ERROR",error)
        }
        
    }
    func loadData()  {
        
        lblNumberDevices.text = "Disponibles: \(arrayData.count)"
        
        if arrayData.count > 0{
            emptyView.isHidden = true
            tableViewDevices.isHidden = false
            tableViewDevices.reloadData()
            
        }else{
            emptyView.isHidden = false
            tableViewDevices.isHidden = true
        }
    }
    
    func getNewDevices(SSID:String) {
        print(SSID)
        requestHelper.getNewDevices(SSID: SSID, completion: { result in
            if result != nil{
                self.arrayData = result!
                self.loadData()
            }else{
                self.alert.showToast(message: "No se pudo conectar con el servidor.", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
            }
        })
    }
    
    func getWiFiName() -> String? {
            var ssid: String?
            if let interfaces = CNCopySupportedInterfaces() as NSArray? {
                for interface in interfaces {
                    if let interfaceInfo = CNCopyCurrentNetworkInfo( interface as! CFString) as NSDictionary? {
                        ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                        break
                    }
                }
            }
            return ssid
    }
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func actionRefreshButton(sender:UIButton){
        getNewDevices(SSID: self.getWiFiName() ?? "")
    }

}
extension AddDevicesViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("NewDeviceAdapterTableViewCell", owner: self, options: nil)?.first  as! NewDeviceAdapterTableViewCell
        let item = arrayData[indexPath.row]
        cell.lblTitleDevice.text = Constantes.getNameDevice(deviceType: item.deviceType)
        cell.lblDescriptionDevice.text = Constantes.getDescriptionDevice(deviceType: item.deviceType)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162.0
    }
    
//    TODO: {{host}}/registerDevices/{{userId}}/{{deviceId}}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "addparentdevicescontroller") as! AddParentDeviceViewController
        vc.data = arrayData[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
