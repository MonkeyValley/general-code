//
//  RegisterUserViewController.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import UIKit
import RealmSwift

class RegisterUserViewController: UIViewController {

    @IBOutlet weak var txtNombres:UITextField!
    @IBOutlet weak var txtApellidos:UITextField!
    @IBOutlet weak var txtPhone:UITextField!
    
    private var user:UserModel?
    private let alert = JojoAlert()
    private let realm = try! Realm()
    private let sessionUser = UserDefaults.standard
    var protocolo:CallbackAddChildUserDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserData()
        initUI()
    }
    
    func initUI() {
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 44.0))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let barButton = UIBarButtonItem(title: "Siguiente", style: .plain, target: self, action: #selector(self.actionReturnPhone))
        toolBar.setItems([flexible, barButton], animated: true)
        self.txtPhone.inputAccessoryView = toolBar
    }
    
    
    @objc func actionReturnPhone(){
        txtPhone.resignFirstResponder()
    }
    
    func getUserData()  {
        let loadedData = sessionUser.object(forKey: "_userData") as! String
        let jsonData = Data(loadedData.utf8)
        do{
            let decoder = JSONDecoder()
            user = try decoder.decode(UserModel.self, from: jsonData)
        }catch let error as NSError{
            debugPrint("LOGIN ERROR",error)
        }
    }
    
    @IBAction func actionRegisterChildUser(_ sender:UIButton){
        
        let name = txtNombres.text!
        let lastname = txtApellidos.text!
        let phone = txtPhone.text!
        if name != "" && lastname != "" && phone != ""{
            let request = RegisterChildUserModel(nombres: name, apellidos: lastname, telefono: phone, mail: user!.mail ?? "")
            requestHelper.rehisterChildUser(userID: user!._id, body: request ) { result in
                if result != nil{
                    if !result!.bError{
                        
                        
                        try! self.realm.write{
                            let model = UserChildModelRealm()
                            guard let childs = result!.user?.childUsers else{return}
                            
                            if let child = childs.last {
                                model._id = child._id
                                model.apellidos = child.apellidos
                                model.nombres = child.nombres
                                model.parentUser = result!.user!._id
                                model.telefono = child.telefono
                                model.tipoUsuario = child.tipoUsuario
                                
                                self.realm.add(model)
                            }else{
                                self.alert.showToast(message: "Error al insertar usuario", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
                            }
                        }
                        
                        self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_SUCCESS, ctx: self)
                        _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { (t) in
                            self.dismiss(animated: true) {
                                self.protocolo?.reloadData()
                            }
                        })
                        
                        
                    }else{
                        self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                    }
                }else{
                    self.alert.showToast(message: "No se pudo conectar con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
                }
            }
        }else{
            self.alert.showToast(message: "Favor de llenar todos los datos.", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
        }
    }
    
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}
extension RegisterUserViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txtNombres){
            txtApellidos.becomeFirstResponder()
        }else if (textField == txtApellidos){
            txtPhone.becomeFirstResponder()
        }else{
            txtPhone.resignFirstResponder()
        }
        
        return true
    }
}
