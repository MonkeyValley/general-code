//
//  AddParentDeviceViewController.swift
//  General Code
//
//  Created by TritonSoft on 17/05/21.
//

import UIKit
import RealmSwift


class AddParentDeviceViewController: UIViewController {

    @IBOutlet weak var lblDiviceName:UILabel!
    @IBOutlet weak var tableViewChilds:UITableView!
    
    var sessionUser = UserDefaults.standard
    var arrayType = [RegisterDeviceModel]()
    var data:NewDevicesModel?
    private var user:UserModel?
    private var alert = JojoAlert()
    private let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iniUI()
        getUserData()
    }
    
    
    func iniUI() {
        setDataAdd()
    }
    
    func setDataAdd()  {
        if let d = data {
            lblDiviceName.text = Constantes.getNameDevice(deviceType: d.deviceType)
            for _ in 0...(d.childs - 1){
                arrayType.append(RegisterDeviceModel(deviceName: "", childType: 0 ))
            }
        }else{
            lblDiviceName.text = ""
        }
    }
    
    func getUserData()  {
        
         let loadedData = sessionUser.object(forKey: "_userData") as! String
        let jsonData = Data(loadedData.utf8)

        do{
            let decoder = JSONDecoder()
            user = try decoder.decode(UserModel.self, from: jsonData)

        }catch let error as NSError{
            debugPrint("LOGIN ERROR",error)
        }
        
    }
    func getItemsToSend() -> [RegisterDeviceModel]{
        var res = [RegisterDeviceModel]()
        arrayType.forEach{ it in
            if(it.childType > 0){
                res.append(it)
            }
        }
        return res
    }
    func getParentDevices() {
        requestHelper.getParentDevices(userId: user!._id) { result in
            if(result != nil){
                try! self.realm.write{
                    let deviceExist = self.realm.objects(ParentDeviceModelRealm.self)
                    let deviceChild = self.realm.objects(ParentChildDevicesModelRealm.self)
                    
                    self.realm.delete(deviceExist)
                    self.realm.delete(deviceChild)
                }
                
                try! self.realm.write{
                    
                    result?.forEach{it in
                        let modelParent = ParentDeviceModelRealm()
                        modelParent.SSID = it.SSID
                        modelParent._id = it._id
                        modelParent.childs = it.childs
                        modelParent.deviceType = it.deviceType
                        modelParent.macAddress = it.macAddress
                        modelParent.status = it.status
                        modelParent.user = it.user
                        
                        it.userDevices.forEach{ child in
                            let modelChild = ParentChildDevicesModelRealm()
                            modelChild._id = child._id
                            modelChild._idParent = it._id
                            modelChild.childId = child.childId
                            modelChild.childType = child.childType ?? 0
                            modelChild.deviceName = child.deviceName ?? ""
                            modelChild.notification = child.notification
                            modelChild.status = child.status
                            modelChild.user = child.user
                            self.realm.add(modelChild)

                        }
                        
                        self.realm.add(modelParent)
                    }
                }
         
            }else{
                self.alert.showToast(message: "No se pudo conectar con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: "xmark.circle.fill", ctx: self)
            }
            
        }
    }
    
    @IBAction func actionRegisterDevice(_ sender:UIButton){
        let arrSender = getItemsToSend()
        requestHelper.sendNewDevicesPost(body: arrSender, userId: user!._id, deviceId: data!._id) { result in
            if result != nil{
                if !result!.bError{
                    self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_SUCCESS, ctx: self)
                    self.getParentDevices()
                    _ = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { it in
                        self.dismiss(animated: true, completion: nil)
                    })
                }else{
                    self.alert.showToast(message: result!.cMensaje, font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
                }
            }else{
                self.alert.showToast(message: "Error al conectarse con el servidor", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_ERROR, ctx: self)
            }
        }
    }
    @IBAction func actionBack(_ sender:UIButton){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController?.popToViewController(viewControllers[0], animated: true)
    }
}
extension AddParentDeviceViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("ItemChildsDevcieTableViewCell", owner: self, options: nil)?.first  as! ItemChildsDevcieTableViewCell
        
        if(arrayType[indexPath.row].childType > 0){
            cell.lblNumberDevice.text = "Dispositivo \(indexPath.row + 1)"
            cell.lblDeviceSelected.text = "\(Constantes.gteEmojiTypeDevice(deviceType: arrayType[indexPath.row].childType)) \(arrayType[indexPath.row].deviceName)"
        }else{
            cell.lblNumberDevice.text = "Dispositivo \(indexPath.row)"
            cell.lblDeviceSelected.text = ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "addchilddevicecontroller") as! AddChildDeviceViewController
        vc.protocolo = self
        vc.indexSelected = indexPath.row
        vc.data = arrayType[indexPath.row]
        self.present(vc, animated: true, completion: nil)
    }
}
extension AddParentDeviceViewController: CallbackSelecetedTypeDeviceDelegate{
    func selectedType(item: RegisterDeviceModel, itemSeleted:Int) {
        print(item)
        
        if itemSeleted >= 0 {
            arrayType[itemSeleted] = item
            tableViewChilds.reloadData()
        }
        
    }
}
