//
//  ViewController.swift
//  General Code
//
//  Created by TritonSoft on 08/05/21.
//

import UIKit
import Lottie

class ViewController: UIViewController {

    @IBOutlet weak var scView: UIScrollView!
    @IBOutlet weak var pc: UIPageControl!
    @IBOutlet weak var btnRegister:UIButton!
    
    private let sessionVar = UserDefaults.standard
    private let alert = JojoAlert()
    private var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    private var pageNumber = 0
    
    var from = "init"
    var sliders = [SliderModel(
                    title: "BIENVENIDO",
                    description: "El Equipo de General code te da la bienvenida al mundo de los smart home.",
                    position: nil,
                    animationName: "hello",
                    backgroundColor: .systemBackground),
                   
                   SliderModel(
                    title: "WIFI",
                    description: "Todos nuestros dispositivos operan con tecnológia WiFi para que puedas acceder a ellos de manera remota.",
                    position: nil,
                    animationName: "wifi",
                    backgroundColor: .systemBackground),
                   SliderModel(
                    title: "SEGURIDAD",
                    description: "Puedes controlar y monitorear los accesos mediante cunetas de usuario.",
                    position:  nil,
                    animationName: "users",
                    backgroundColor: .systemBackground)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSc()
    }

    func initSc(){
        
        if(sliders.count > 1){
            btnRegister.isHidden = true
        }
        
        if (from == "init"){
            btnRegister.setTitle("Registrame", for: .normal)
        }else{
            btnRegister.setTitle("Cerrar", for: .normal)
        }
        
        btnRegister.layer.cornerRadius = 10
        for subview : UIView in self.scView.subviews{
            subview.removeFromSuperview()
        }
        pc.currentPage = 0
        pc.numberOfPages = sliders.count
        scView.contentSize = CGSize(width: (self.view.frame.size.width * CGFloat(sliders.count)), height: self.scView.frame.size.height)
        for (index, item ) in sliders.enumerated() {
            let position = CGPoint(x: (self.view.frame.size.width) * CGFloat(index), y: 0)
            item.position = position
            scView.addSubview(setUiToSlider(item: item))
            
        }
        
    }
    
    func setUiToSlider(item:SliderModel) -> UIView {
        
        let bodySlider = UIView()
        let buttonNext = UIButton()
        let titleLabel = UILabel()
        let descriptionLabel = UILabel()

        
        let width = self.view.frame.width
        let height = self.scView.frame.height
        
        bodySlider.frame = CGRect(x: 0, y: 0, width: width , height: height  )
        bodySlider.frame.origin = item.position!
        bodySlider.backgroundColor = item.backgroundColor

        
        let lottieAnim = AnimationView.init(name: item.animationName)
        lottieAnim.frame = CGRect(x: 30 , y: 100, width: width - 60 , height: 200)
        lottieAnim.contentMode = .scaleAspectFill
        lottieAnim.animationSpeed = 0.5
        lottieAnim.loopMode = .loop
        lottieAnim.play()
        
        buttonNext.frame = CGRect(x: width - 116 , y: height - 65, width: 100, height: 45)
        buttonNext.setTitle("Siguiente", for: .normal)
        buttonNext.layer.cornerRadius = 10
        buttonNext.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        buttonNext.backgroundColor = .red
        buttonNext.setTitleColor(.white, for: .normal)
        buttonNext.addTarget(self, action: #selector(self.nextSlide), for: .touchUpInside)
        
        let titleHeight = stringH(size: 19.0 , str: item.title)
        titleLabel.frame = CGRect(x: 16, y: lottieAnim.frame.origin.y + 240, width: self.view.frame.width - 32, height: titleHeight)
        titleLabel.font = UIFont.systemFont(ofSize: 19.0, weight: .semibold)
        titleLabel.text = item.title
        titleLabel.textColor = .label
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        
        let descHeight = stringH(size: 14.0 , str: item.description) + 20
        descriptionLabel.frame = CGRect(x: 16, y: titleLabel.frame.origin.y + titleHeight + 8, width: self.view.frame.width - 32, height: descHeight)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
        descriptionLabel.text = item.description
        descriptionLabel.textColor = .label
        descriptionLabel.textAlignment = .center
        
        
        bodySlider.addSubview(lottieAnim)
        bodySlider.addSubview(titleLabel)
        bodySlider.addSubview(descriptionLabel)
        
        return bodySlider
    }
    
    @objc func nextSlide(){
        
        self.scView.setContentOffset(CGPoint(x: CGFloat(pageNumber + 1) * view.frame.size.width, y: 0), animated: true)
    }
    
    func stringW(size:CGFloat, str:String) -> CGFloat {
        let constraintRect = CGSize(width: UIScreen.main.bounds.width, height: .greatestFiniteMagnitude)
        let boundingBox = str.trimmingCharacters(in: .whitespacesAndNewlines).boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: size)], context: nil)
        return boundingBox.width
    }
    
    func stringH(size:CGFloat, str:String) -> CGFloat {
        let constraintRect = CGSize(width: UIScreen.main.bounds.width, height: .greatestFiniteMagnitude)
        let boundingBox = str.trimmingCharacters(in: .whitespacesAndNewlines).boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: size)], context: nil)
        return boundingBox.height
    }
    
    @IBAction func actionRegister(_ sender:UIButton){
        alert.showAlertLoading(on: self)
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.register), userInfo: nil, repeats: false)
    }
    
    @objc func register(){
        
        if(from == "init"){
        sessionVar.setValue(true, forKey: "_onBoarding")
        sessionVar.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "registerController") as! RegiterViewController
        self.present(vc, animated: true, completion: nil)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
}


extension ViewController: UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageNumber = Int(scView.contentOffset.x / self.view.frame.size.width)
        pc.currentPage = pageNumber
        
        if(pageNumber == (sliders.count - 1)){
            btnRegister.isHidden = false
        }else{
            btnRegister.isHidden = true
        }
        
        
        
    }
}
