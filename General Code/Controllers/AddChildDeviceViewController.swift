//
//  AddChildDeviceViewController.swift
//  General Code
//
//  Created by TritonSoft on 17/05/21.
//

import UIKit

class AddChildDeviceViewController: UIViewController {

    @IBOutlet weak var collectionDevices:UICollectionView!
    @IBOutlet weak var txtNameDevice:UITextField!
    
    private var collectionviewFlowLayout:UICollectionViewFlowLayout!
    
    private let alert = JojoAlert()
    var protocolo:CallbackSelecetedTypeDeviceDelegate?
    var protocoloEdit:CallbackEditTypeDeviceDelegate?
    var indexSelected:Int = -1
    private var itemSeleted = 0
    var data:RegisterDeviceModel!
    var action = ""
    private var arrayItems:[RegisterDeviceModel] = []
    private var arraySelected:[Bool] = [false, false, false, false]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        initUi()
    }
    
    func initUi()  {
        arrayItems.append( RegisterDeviceModel(deviceName: "Portón", childType: 1))
        arrayItems.append( RegisterDeviceModel(deviceName: "Puerta", childType: 2))
        arrayItems.append( RegisterDeviceModel(deviceName: "Pistón", childType: 3))
        arrayItems.append( RegisterDeviceModel(deviceName: "Corredizo", childType: 4))

        if(data != nil){
            txtNameDevice.text = data.deviceName
            
            for index in 0...arrayItems.count - 1{
                let item = arrayItems[index]
                if(item.childType == data.childType){
                    arraySelected[index] = true
                    itemSeleted = item.childType
                    collectionDevices.reloadData()
                }
            }
        }
        
    }
    func setupCollectionView() {
        
        let nib = UINib(nibName: "ItemChildDevicesCollectionViewCell", bundle: nil)
        collectionDevices.register(nib, forCellWithReuseIdentifier: "itemtypedevice")
    
    }
    
    override func viewWillLayoutSubviews() {
        setupCollectionViewItemSize()
    }
    func setupCollectionViewItemSize() {
        if collectionviewFlowLayout == nil {
            let numberitems:CGFloat = 2
            let lineSpacing:CGFloat = 5
            let interspacign:CGFloat = 5
            
            print(collectionDevices.frame.width )
            
            let widthF:CGFloat  = ((collectionDevices.frame.width - (numberitems - 1) * interspacign) / numberitems) - 20
            let heightF:CGFloat  = 90.0
            
            print(widthF)
            print(heightF)
            
            
            collectionviewFlowLayout = UICollectionViewFlowLayout()
            
            collectionviewFlowLayout.itemSize = CGSize(width: widthF, height: heightF)
            collectionviewFlowLayout.sectionInset = .zero
            collectionviewFlowLayout.scrollDirection = .vertical
            collectionviewFlowLayout.minimumLineSpacing = lineSpacing
            collectionviewFlowLayout.minimumInteritemSpacing = interspacign
            
            
            collectionDevices.setCollectionViewLayout(collectionviewFlowLayout, animated: true)
        }
    }
    @objc func closeKeyboard(){
        view.endEditing(true)
    }
    @IBAction func actionAddChildDevice(_ sender:UIButton){
        if(itemSeleted > 0 && txtNameDevice.text != ""){
            self.dismiss(animated: true, completion: {
                
                let sender = RegisterDeviceModel(deviceName: self.txtNameDevice.text!, childType: self.itemSeleted)
                if(self.protocolo != nil){
                    self.protocolo?.selectedType(item: sender, itemSeleted:self.indexSelected)
                }else if self.protocoloEdit != nil {
                    self.protocoloEdit?.selectedTypeEdit(item: sender, action: self.action, itemSeleted: self.indexSelected)
                }
            })
        }else{
            alert.showToast(message: "Faltan algunos datos", font: UIFont.systemFont(ofSize: 14.0, weight: .bold), UIIcon: Constantes.ICON_INFO, ctx: self)
        }
    }
    @IBAction func actionClose(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}
extension AddChildDeviceViewController:UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemtypedevice", for: indexPath) as! ItemChildDevicesCollectionViewCell
        
        cell.lblNameDevice.text = arrayItems[indexPath.row].deviceName
        cell.lblIconDevice.text = Constantes.gteEmojiTypeDevice(deviceType:arrayItems[indexPath.row].childType)
        
        if(arraySelected[indexPath.row]){
            cell.borderColor = .link
            cell.borderWidth = 1
            cell.cornerRadius = 10
        }else{
            cell.borderColor = .none
            cell.borderWidth = 0.0
            cell.cornerRadius = 10
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemSeleted = arrayItems[indexPath.row].childType
        clear()
        arraySelected[indexPath.row] = true
        collectionView.reloadData()
    }
    
    func clear() {
        for (i, _) in arrayItems.enumerated(){
            arraySelected[i] = false
        }
    }
}
extension AddChildDeviceViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.closeKeyboard()
        return true
    }
}
