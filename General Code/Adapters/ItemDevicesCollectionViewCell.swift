//
//  ItemDevicesCollectionViewCell.swift
//  General Code
//
//  Created by TritonSoft on 11/05/21.
//

import UIKit

class ItemDevicesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewStatusConected:UIView!
    @IBOutlet weak var lblNameDevice:UILabel!
    @IBOutlet weak var icTypeDevice:UILabel!
    @IBOutlet weak var lblStatusDevice:UILabel!
    @IBOutlet weak var lblStatusConected:UILabel!

}
