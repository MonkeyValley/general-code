//
//  ItemChildDevicesCollectionViewCell.swift
//  General Code
//
//  Created by TritonSoft on 19/05/21.
//

import UIKit

class ItemChildDevicesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblNameDevice:UILabel!
    @IBOutlet weak var lblIconDevice:UILabel!
    @IBOutlet weak var backgroundItem:UIView!
}
