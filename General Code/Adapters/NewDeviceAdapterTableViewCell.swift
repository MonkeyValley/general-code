//
//  NewDeviceAdapterTableViewCell.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//

import UIKit

class NewDeviceAdapterTableViewCell: UITableViewCell {

    @IBOutlet weak var icDevice:UIImageView!
    @IBOutlet weak var lblTitleDevice:UILabel!
    @IBOutlet weak var lblDescriptionDevice:UILabel!
    @IBOutlet weak var lblTecnologyDevice:UILabel!
    @IBOutlet weak var lblType:UILabel!
}
