//
//  DeviceAdapterTableViewCell.swift
//  General Code
//
//  Created by TritonSoft on 14/05/21.
//

import UIKit

class DeviceAdapterTableViewCell: UITableViewCell {

    @IBOutlet weak var icDevice:UIImageView!
    @IBOutlet weak var lblDeviceName:UILabel!
    @IBOutlet weak var lblWifiName:UILabel!
    @IBOutlet weak var lblNumbreDevices:UILabel!
    @IBOutlet weak var lblStatus:UILabel!
    @IBOutlet weak var viewStatus:UIView!
    
}
