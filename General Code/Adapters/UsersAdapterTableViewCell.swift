//
//  UsersAdapterTableViewCell.swift
//  General Code
//
//  Created by TritonSoft on 14/05/21.
//

import UIKit

class UsersAdapterTableViewCell: UITableViewCell {

    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var imgUser:UIImageView!
}
