//
//  SliderModel.swift
//  General Code
//
//  Created by TritonSoft on 09/08/21.
//

import Foundation
import UIKit

class SliderModel {
    var title:String = ""
    var description:String = ""
    var position:CGPoint? = nil
    var animationName:String = ""
    var backgroundColor:UIColor? = nil
    init(title:String, description:String, position:CGPoint?, animationName:String, backgroundColor:UIColor?) {
        self.title = title
        self.description = description
        self.animationName = animationName
        self.backgroundColor = backgroundColor
        self.position = position
    }
}
