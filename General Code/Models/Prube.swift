//
//  Prube.swift
//  General Code
//
//  Created by TritonSoft on 10/05/21.
//

import Foundation

class Prube: Decodable {
    var name:String?
    var lastname:String?
    var phone:String?
    var mail:String?
    var address:String?
    var _id:String?
}
