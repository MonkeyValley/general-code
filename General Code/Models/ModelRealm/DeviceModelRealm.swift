//
//  DeviceModelRealm.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//


import Foundation
import RealmSwift

class DeviceModelRelm: Object {
    @objc dynamic var _id: String = ""
    @objc dynamic var user: String = ""
    @objc dynamic var deviceName:String = ""
    @objc dynamic var childType: Int = 0
    @objc dynamic var notification: Bool = false
    @objc dynamic var status: Int = 0
    @objc dynamic var sensor: Bool = false
    @objc dynamic var childId: Int = 0
    @objc dynamic var lastAccess: String?
    @objc dynamic var lastUserAccess:String?

}
