//
//  UserChildModelRealm.swift
//  General Code
//
//  Created by TritonSoft on 20/05/21.
//

import Foundation
import RealmSwift

class UserChildModelRealm: Object {
    @objc dynamic var _id:String = ""
    @objc dynamic var nombres:String = ""
    @objc dynamic var apellidos:String = ""
    @objc dynamic var telefono:String = ""
    @objc dynamic var tipoUsuario:Int = 0
    @objc dynamic var parentUser:String = ""
}
