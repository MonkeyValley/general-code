//
//  ParentDeviceModelRealm.swift
//  General Code
//
//  Created by TritonSoft on 19/05/21.
//

import Foundation
import RealmSwift
import UIKit

class ParentDeviceModelRealm: Object {
    @objc dynamic var _id:String = ""
    @objc dynamic var macAddress:String = ""
    @objc dynamic var deviceType:Int = 0
    @objc dynamic var SSID:String = ""
    @objc dynamic var status:Int = 0
    @objc dynamic var childs:Int = 0
    @objc dynamic var user:String = ""
}
