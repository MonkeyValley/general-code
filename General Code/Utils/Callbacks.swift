//
//  Callbacks.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//

import Foundation

protocol DeviceDetailsDelegate {
    func reloadHome()
}

protocol ResultScannDelegate {
    func response(res:Bool, userid:String)
}

protocol CallbackResultDelegate {
    func resultRegisterAddres(address:AddressModel)
}

protocol ScannAgainDelegate {
    func scann()
}

protocol CallbackSelecetedTypeDeviceDelegate {
    func selectedType(item:RegisterDeviceModel, itemSeleted:Int)
}

protocol CallbackEditTypeDeviceDelegate {
    func selectedTypeEdit(item:RegisterDeviceModel, action:String, itemSeleted:Int)
}

protocol CallbackAddChildUserDelegate {
    func reloadData()
}

protocol CallbackReloadDataDelegate {
    func reloadData()
}
