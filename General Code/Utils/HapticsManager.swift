//
//  HapticsManager.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//

import UIKit

final class HapticsManager{
    
    static let shared = HapticsManager()
    private init (){}
    
    public func selectionVibrate(){
        DispatchQueue.main.async {
            let selectionFeedbackGeneretor = UISelectionFeedbackGenerator()
            selectionFeedbackGeneretor.prepare()
            selectionFeedbackGeneretor.selectionChanged()
        }
    }
    
    public func vibrate(for type:UINotificationFeedbackGenerator.FeedbackType){
        DispatchQueue.main.async {
            let notificationGenerator = UINotificationFeedbackGenerator()
            notificationGenerator.prepare()
            notificationGenerator.notificationOccurred(type)
        }
    }
}
