//
//  ActionDeviceModel.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//

import Foundation

struct ActionDeviceModel:Codable {
    var bError:Bool?
    var cMensaje:String?
    var action:Int?
}
