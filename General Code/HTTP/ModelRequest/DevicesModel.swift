//
//  BaseDevices.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import Foundation

struct DevicesModel:Codable{
    var _id:String
    var user:String
    var deviceName:String?
    var childType:Int?
    var notification:Bool
    var status:Int
    var sensor:Bool
    var childId:Int
    var lastUserAccess:String?
}
