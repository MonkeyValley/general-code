//
//  RegisterDeviceModel.swift
//  General Code
//
//  Created by TritonSoft on 19/05/21.
//

import Foundation

struct RegisterDeviceModel:Codable {
    var deviceName:String
    var childType:Int
}
