//
//  BaseSetPasswordGuestUserModel.swift
//  General Code
//
//  Created by TritonSoft on 23/05/21.
//

import Foundation

struct BaseSetPasswordGuestUserModel:Codable {
    var bError:Bool
    var cMensaje:String
    var user:UserModel?
}
