//
//  BaseRegisterAddress.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import Foundation

struct BaseRegisterUser: Codable{
    var bError:Bool
    var cMensaje:String
    var user:UserModel?
    var direccion:AddressModel?
}
