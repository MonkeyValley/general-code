//
//  RegisterChildUserModel.swift
//  General Code
//
//  Created by TritonSoft on 20/05/21.
//

import Foundation

struct RegisterChildUserModel:Codable {
    var nombres:String
    var apellidos:String
    var telefono:String
    var mail:String
}
