//
//  BaseResetPasswordModel.swift
//  General Code
//
//  Created by TritonSoft on 22/05/21.
//

import Foundation

struct BaseResetPasswordModel:Codable{
    var bError:Bool
    var cMensaje:String?
    var data:ResetPasswordModel?
    var _id:String?
}
