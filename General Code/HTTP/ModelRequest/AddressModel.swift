//
//  AddressModel.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import Foundation

struct AddressModel: Codable {
    var _id: String
    var nombreDireccion: String
    var nombreCalle: String
    var numeroExterior: String
    var numeroInterior: String
    var referencias: String
    var colonia: String
    var _lat: Double
    var _lng: Double
    var user: String
}
