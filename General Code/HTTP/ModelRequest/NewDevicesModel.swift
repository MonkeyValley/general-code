//
//  NewDevicesModel.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//

import Foundation

struct NewDevicesModel:Codable {
    var _id:String
    var macAddress:String
    var deviceType:Int
    var SSID:String
    var status:Int
    var childs:Int
    var remoteAddress:String?
    var user:String?
}
