//
//  NotificationRequest.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//

import Foundation

struct NotificationModel: Codable {
    var idMotor:String
    var action:Int
}
