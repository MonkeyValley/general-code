//
//  ParentDevice.swift
//  General Code
//
//  Created by TritonSoft on 19/05/21.
//

import Foundation

struct ParentDeviceModel:Codable {
    
    var _id:String
    var macAddress:String
    var deviceType:Int
    var SSID:String
    var status:Int
    var childs:Int
    var user:String
    var userDevices:[DevicesModel]
        
}
