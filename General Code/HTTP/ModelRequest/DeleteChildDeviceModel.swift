//
//  DeleteChildDeviceModel.swift
//  General Code
//
//  Created by TritonSoft on 21/05/21.
//

import Foundation

struct DeleteChildDeviceModel:Codable {
    var bError:Bool
    var cMensaje:String
    var action:Int?
}
