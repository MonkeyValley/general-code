//
//  LoginModel.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import Foundation

struct UserModel: Codable {
    var lstDirecciones:[AddressModel]?
    var  childUsers: [UserModel]?
    var _id:String
    var nombres:String
    var apellidos:String
    var mail:String?
    var telefono:String
    var tipoUsuario:Int
    var maxUsers:Int?
    var playerId:String?
    var mailAlexa:String?
}
