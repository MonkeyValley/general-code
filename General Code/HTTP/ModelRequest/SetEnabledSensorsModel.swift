//
//  SetEnabledSensorsModel.swift
//  General Code
//
//  Created by Cristian Canedo on 01/02/22.
//

import Foundation

struct SetEnabledSensorsModelResponse:Codable{
    var bError:Bool
}
