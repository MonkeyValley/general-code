//
//  ResetPasswordModel.swift
//  General Code
//
//  Created by TritonSoft on 22/05/21.
//

import Foundation

struct ResetPasswordModel:Codable {
    var cMensaje:String
    var bError:Bool
}
