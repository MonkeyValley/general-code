//
//  SetPasswordGuestUserRequest.swift
//  General Code
//
//  Created by TritonSoft on 23/05/21.
//

import Foundation
import Alamofire

func SetPasswordGuestUserRequest(userId:String, password:String, playerId:String, completion: @escaping (BaseSetPasswordGuestUserModel?) -> Void){
//   {{host}}/setPasswordGuestUser/{{userId}}
    let URL = "\(Constantes.SERVER_URL)/setPasswordGuestUser/\(userId)"
    let params:Parameters = ["password": password,
                             "playerId":playerId]
    AF.request(URL, method: .post, parameters: params ).responseJSON { response in
        do {
            debugPrint(URL,response)
            let responseServer = try JSONDecoder().decode(BaseSetPasswordGuestUserModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}


