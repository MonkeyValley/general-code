//
//  RegisterUserRequest.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import Foundation
import Alamofire

func RegisterUserRequest(nombre:String, apellidos:String, email:String, telefono:String, password:String, playerId:String, direccion:AddressModel, completion: @escaping (BaseRegisterUser?) -> Void){

    let URL = Constantes.SERVER_URL + "/registerUser"
    let params : Parameters = ["nombres":nombre,
                               "apellidos":apellidos,
                               "mail":email,
                               "telefono":telefono,
                               "password":password,
                               "playerId":playerId,
                               "direccion":[ "nombreDireccion":direccion.nombreDireccion,
                                             "nombreCalle":direccion.nombreCalle,
                                             "numeroExterior":direccion.numeroExterior,
                                             "numeroInterior":direccion.numeroInterior,
                                             "referencias":direccion.referencias,
                                             "colonia":direccion.colonia,
                                             "_lat":direccion._lat,
                                             "_lng":direccion._lng,
                                             "user":direccion.user,
                                             "_id":direccion._id]]
    
    AF.request(URL, method: .post,  parameters: params).responseJSON { response in
        do {
            let responseServer = try JSONDecoder().decode(BaseRegisterUser.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
