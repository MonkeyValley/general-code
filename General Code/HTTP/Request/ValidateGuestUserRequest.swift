//
//  ValidateGuestUserRequest.swift
//  General Code
//
//  Created by TritonSoft on 23/05/21.
//

import Foundation
import Alamofire

func ValidateGuestUserRequest(idGuestUser:String, completion: @escaping (BaseValidateGuestUser?) -> Void){
//    {{host}}/validateGuestUser/{{IdGuestUser}}
        let URL = "\(Constantes.SERVER_URL)/validateGuestUser/\(idGuestUser)"
            AF.request(URL, method: .get).responseJSON { response in
                do {
                    debugPrint(URL,response)
                    let responseServer = try JSONDecoder().decode(BaseValidateGuestUser.self, from: response.data!)
                    completion(responseServer)
                } catch let error as NSError {
                    debugPrint(URL,error)
                    completion(nil)
                }
            }
}

