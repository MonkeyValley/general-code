//
//  ListDevicesGet.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//


import Foundation
import Alamofire

func ListDevicesGetRequest(userId:String,completion: @escaping ([DevicesModel]?) -> Void){

    let URL = Constantes.SERVER_URL + "/getDevices/" + userId
    
    AF.request(URL, method: .get).responseJSON { response in
        do {
            print(response)
            let responseServer = try JSONDecoder().decode([DevicesModel].self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}


