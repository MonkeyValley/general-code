//
//  SetEnabledSensorsRequest.swift
//  General Code
//
//  Created by Cristian Canedo on 01/02/22.
//

import Foundation
import Alamofire


func SetEnabledSensorsRequest(deviceId:String, isEnabled:Bool, completion:@escaping(SetEnabledSensorsModelResponse?) -> Void){
    
    let URL = "\(Constantes.SERVER_URL)/updateSensor/\(deviceId)/\(isEnabled)"

    AF.request(URL, method: .get ).responseJSON { response in
        do {
            debugPrint(URL,response)
            let responseServer = try JSONDecoder().decode(SetEnabledSensorsModelResponse.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
    
}
