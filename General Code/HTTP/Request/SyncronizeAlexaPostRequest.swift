//
//  SyncronizeAlexaPostRequest.swift
//  General Code
//
//  Created by TritonSoft on 20/05/21.
//
import Foundation
import Alamofire

func SyncronizeAlexaPostRequest(userId:String, mail:String, completion: @escaping (SyncronizeAlexaModel?) -> Void){

//    {{host}}/registerUserAlexa/{{userId}}/{{mail}}
    let URL = "\(Constantes.SERVER_URL)/registerUserAlexa/\(userId)/\(mail)"

    AF.request(URL, method: .get).responseJSON { response in
        do {
            debugPrint(URL,response)
            let responseServer = try JSONDecoder().decode(SyncronizeAlexaModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
