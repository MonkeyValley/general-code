//
//  ActionDeviceRequest.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//

import Foundation
import Alamofire

func ActionDeviceRequest(userDevice:String, stauts:Int, completion: @escaping (ActionDeviceModel?) -> Void){

    let URL = "\(Constantes.SERVER_URL)/deviceAction/\(userDevice)/\(stauts)"
    
    AF.request(URL, method: .get).responseJSON { response in
        do {
            let responseServer = try JSONDecoder().decode(ActionDeviceModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
