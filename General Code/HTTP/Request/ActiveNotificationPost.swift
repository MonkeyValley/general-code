//
//  ActiveNotificationPost.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//
import Foundation
import Alamofire

func ActiveNotificationPost(macAddress:String, stauts:Bool, completion: @escaping (NotificationModel?) -> Void){

    let URL = "\(Constantes.SERVER_URL)/notificationDevice/\(macAddress)/\(stauts)"
    
    AF.request(URL, method: .get).responseJSON { response in
        do {
            let responseServer = try JSONDecoder().decode(NotificationModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
