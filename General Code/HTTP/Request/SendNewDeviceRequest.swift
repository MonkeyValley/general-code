//
//  SendNewDeviceRequestModel.swift
//  General Code
//
//  Created by TritonSoft on 19/05/21.
//

import Foundation
import Alamofire

func SendNewDeviceRequest(userId:String, deviceId:String, body:[RegisterDeviceModel], completion: @escaping (BaseRegisterNewDeviceModel?) -> Void){

//    {{host}}/registerDevices/{{userId}}/{{deviceId}}
    let filePath = "\(Constantes.SERVER_URL)/registerDevices/\(userId)/\(deviceId)"

    var arraySend:[[String:Any]] = []
    
    body.forEach{it in
        let object:[String:Any] = ["deviceName": it.deviceName, "childType": it.childType]
        arraySend.append(object)
    }
    
    
    do {
        
        var request = URLRequest(url: URL(string: filePath)!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let parameters: [String: Any] = [
            "devices": arraySend
        ]

        let jsonParams = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        request.httpBody = jsonParams
        print(jsonParams)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                print("error", error ?? "Unknown error")
                debugPrint(filePath,error)
                completion(nil)
                return
            }

            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                debugPrint(filePath,error)
                completion(nil)
                return
            }

            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(responseString)")
            do {
                debugPrint(filePath,response)
                
                let responseServer = try JSONDecoder().decode(BaseRegisterNewDeviceModel.self, from: data)
                completion(responseServer)
            } catch let error as NSError {
                debugPrint(filePath,error)
                completion(nil)
            }
        }

        task.resume()

    }catch let error as NSError{
        debugPrint(filePath,error)
        completion(nil)
    }
    
/*    body.forEach{ it in
        let para = Parameters.init(dictionaryLiteral: ("deviceName", it.deviceName),("childType", it.childType))
        arrayParams.append(para)
    }
    let parameters:Parameters = ["devices":arrayParams]
    print(parameters)
    
    
    AF.request(filePath, method: .post, parameters: parameters ).responseJSON { response in
        do {
            debugPrint(filePath,response)
            
            let responseServer = try JSONDecoder().decode(BaseRegisterNewDeviceModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(filePath,error)
            completion(nil)
        }
    }
*/
   
    
}

extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
