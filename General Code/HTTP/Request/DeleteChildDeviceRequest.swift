//
//  DeleteChildDeviceRRequest.swift
//  General Code
//
//  Created by TritonSoft on 21/05/21.
//

import Foundation
import Alamofire

func DeleteChildDeviceRequest(deviceId:String, completion: @escaping (DeleteChildDeviceModel?) -> Void){
//    {{host}}/deleteUserDevice/{{deviceId}}
    let URL = "\(Constantes.SERVER_URL)/deleteUserDevice/\(deviceId)/"


    AF.request(URL, method: .get).responseJSON { response in
        do {
            debugPrint(URL,response)
            let responseServer = try JSONDecoder().decode(DeleteChildDeviceModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
