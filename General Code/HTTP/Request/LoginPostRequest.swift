//
//  LoginPostRequest.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import Foundation
import Alamofire

func LoginPostRequest(telefono:String, password:String, playerId:String, completion: @escaping (BaseLogin?) -> Void){
    
    let URL = Constantes.SERVER_URL + "/login"
    let params : Parameters = ["telefono":telefono,"password":password, "playerId":playerId]
    
    AF.request(URL, method: .post,  parameters: params).responseJSON { response in
        do {
            let responseServer = try JSONDecoder().decode(BaseLogin.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
