//
//  ResetPasswordRequest.swift
//  General Code
//
//  Created by TritonSoft on 22/05/21.
//

import Foundation
import Alamofire


func ResetPasswordPostRequest(mail:String, completion: @escaping (BaseResetPasswordModel?) -> Void){
//    {{host}}/resetPassword
    let URL = "\(Constantes.SERVER_URL)/resetPassword"

    let params:Parameters = ["cMail":mail]
        AF.request(URL, method: .post, parameters: params).responseJSON { response in
            do {
                debugPrint(URL,response)
                let responseServer = try JSONDecoder().decode(BaseResetPasswordModel.self, from: response.data!)
                completion(responseServer)
            } catch let error as NSError {
                debugPrint(URL,error)
                completion(nil)
            }
        }
}
