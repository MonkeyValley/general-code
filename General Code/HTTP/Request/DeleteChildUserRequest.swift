//
//  DeleteChildUserRequest.swift
//  General Code
//
//  Created by TritonSoft on 21/05/21.
//

import Foundation
import Alamofire

func DeleteChildUserRequest(userId:String, completion: @escaping (BaseLogin?) -> Void){
//    {{host}}/deleteGuestUser/{{userId}}
    let URL = "\(Constantes.SERVER_URL)/deleteGuestUser/\(userId)/"


    AF.request(URL, method: .get).responseJSON { response in
        do {
            debugPrint(URL,response)
            let responseServer = try JSONDecoder().decode(BaseLogin.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
