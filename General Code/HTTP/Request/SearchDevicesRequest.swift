//
//  SearchDevicesRequest.swift
//  General Code
//
//  Created by TritonSoft on 22/05/21.
//

import Foundation
import Alamofire

func SearchDevicesRequest(userId:String, completion: @escaping (SearchDevicesModel?) -> Void){
//    {{host}}/searchDevices/{{userId}}
    let URL = "\(Constantes.SERVER_URL)/searchDevices/\(userId)/"


    AF.request(URL, method: .get).responseJSON { response in
        do {
            debugPrint(URL,response)
            let responseServer = try JSONDecoder().decode(SearchDevicesModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
