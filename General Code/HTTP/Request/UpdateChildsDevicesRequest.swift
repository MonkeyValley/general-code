//
//  EditParentDeviceRequest.swift
//  General Code
//
//  Created by TritonSoft on 25/05/21.
//

import Foundation
import Alamofire

func UpdateChildsDevicesRequest(userId:String, body:RegisterDeviceModel , completion: @escaping (BaseUpdateChildDeviceModel?) -> Void){
//   {{host}}/updateChidDevice/{{userId}}
    let URL = "\(Constantes.SERVER_URL)/updateChidDevice/\(userId)"
    let params:Parameters = ["deviceName": body.deviceName,
                             "childType":body.childType]
    AF.request(URL, method: .post, parameters: params ).responseJSON { response in
        do {
            debugPrint(URL,response)
            let responseServer = try JSONDecoder().decode(BaseUpdateChildDeviceModel.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}


