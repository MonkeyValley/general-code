//
//  RegisterChildUserRequest.swift
//  General Code
//
//  Created by TritonSoft on 20/05/21.
//

import Foundation
import Alamofire

func RegisterChildUserRequest(userId:String, body:RegisterChildUserModel, completion: @escaping (BaseLogin?) -> Void){

//    {{host}}/registerGuestUser/{{userId}}
    let URL = "\(Constantes.SERVER_URL)/registerGuestUser/\(userId)/"
    let params:Parameters = [ "nombres": body.nombres,
                              "apellidos": body.apellidos,
                              "telefono": body.telefono,
                              "mail": body.mail]

    AF.request(URL, method: .post, parameters:params).responseJSON { response in
        do {
            debugPrint(URL,response)
            let responseServer = try JSONDecoder().decode(BaseLogin.self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
