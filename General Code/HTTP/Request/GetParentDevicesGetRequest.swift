//
//  GetParentDevices.swift
//  General Code
//
//  Created by TritonSoft on 19/05/21.
//
import Foundation
import Alamofire

func GetParentDevicesGetRequest(userId:String, completion: @escaping ([ParentDeviceModel]?) -> Void){

    let URL = "\(Constantes.SERVER_URL)/getParentDevices/\(userId)"
    AF.request(URL, method: .get).responseJSON { response in
        do {
//            debugPrint(URL,response)
            
            let responseServer = try JSONDecoder().decode([ParentDeviceModel].self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
