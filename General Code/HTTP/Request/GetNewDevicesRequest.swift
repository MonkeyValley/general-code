//
//  GetNewDevicesRequest.swift
//  General Code
//
//  Created by TritonSoft on 16/05/21.
//

import Foundation


import Foundation
import Alamofire

func GetNewDevicesRequest(SSID:String, completion: @escaping ([NewDevicesModel]?) -> Void){

    let SSIDSECURE = SSID.replacingOccurrences(of: " ", with: "%20")
    let URL = "\(Constantes.SERVER_URL)/getNewDevices/\(SSIDSECURE)"
    
    AF.request(URL, method: .get).responseJSON { response in
        do {
            debugPrint(URL,response)
            
            let responseServer = try JSONDecoder().decode([NewDevicesModel].self, from: response.data!)
            completion(responseServer)
        } catch let error as NSError {
            debugPrint(URL,error)
            completion(nil)
        }
    }
}
