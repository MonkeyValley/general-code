//
//  ValidateResetPassword.swift
//  General Code
//
//  Created by TritonSoft on 22/05/21.
//

import Foundation
import Alamofire

func ValidateResetPasswordRequest(_id:String, code:String, newPassword:String, completion: @escaping (BaseValidatePin?) -> Void){
    //    {{host}}/validateResetPassword
        let URL = "\(Constantes.SERVER_URL)/validateResetPassword"
        let params:Parameters = ["_id":_id, "code":code, "newPassword": newPassword]
            AF.request(URL, method: .post, parameters: params).responseJSON { response in
                do {
                    debugPrint(URL,response)
                    let responseServer = try JSONDecoder().decode(BaseValidatePin.self, from: response.data!)
                    completion(responseServer)
                } catch let error as NSError {
                    debugPrint(URL,error)
                    completion(nil)
                }
            }
}
