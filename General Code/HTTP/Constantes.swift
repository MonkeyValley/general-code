//
//  Constantes.swift
//  General Code
//
//  Created by TritonSoft on 15/05/21.
//

import Foundation
import Alamofire
enum Constantes {
    static let SERVER_URL = "https://general-code.tritonsoft.tech"
    
    static let STATUS_OFFLINE  = 0
    static let STATUS_ABIERTO  = 1
    static let STATUS_CERRADO  = 2
    
    static let STATUS_OFFLINE_EMOJI  = "🔐"
    static let STATUS_ABIERTO_EMOJI  = "🔓"
    static let STATUS_CERRADO_EMOJI  = "🔒"
    
    static let STATUS_EMOJI_CHILD_TYPE_1 = "🏡"
    static let STATUS_EMOJI_CHILD_TYPE_2 = "🚪"
    static let STATUS_EMOJI_CHILD_TYPE_3 = "🏠"
    static let STATUS_EMOJI_CHILD_TYPE_4 = "🏠"
    
    static let ICON_ERROR = "xmark.circle.fill"
    static let ICON_INFO = "info.circle.fill"
    static let ICON_SUCCESS = "checkmark.circle.fill"
    
    static  let GENERAL_CODE_DUAL = 1
    
    static func getNameDevice(deviceType:Int) -> String
    {
        switch (deviceType)
        {
            case GENERAL_CODE_DUAL:
                return "General Code Dual";
            default:
                return "-1";
        }

    }

    static func getDescriptionDevice(deviceType:Int) -> String
    {
        switch (deviceType)
        {
            case GENERAL_CODE_DUAL:
                return "Con este dispositivo podras controlar de manera remota hasta 2 dispositivos ";
            default:
                return "-1";
        }

    }
    
    
    static func gteEmojiTypeDevice(deviceType:Int) -> String
    {
        switch (deviceType)
        {
        case 1:
            return "🏡"
        case 2:
            return "🚪"
        case 3:
            return "🏠"
        case 4:
            return "🏠"
        default:
            return "";
        }

    }
}
